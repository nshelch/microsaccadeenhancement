%% Figure 2A: Heatmaps
load('./Data/across_subjects.mat')
pxAngle =  0.5278;
limit = 40;
histResultLeft = nanmean(indv.gaze_heatmap_left, 3)./max(max(nanmean(indv.gaze_heatmap_left,3)));
histResultRight = nanmean(indv.gaze_heatmap_right, 3)./max(max(nanmean(indv.gaze_heatmap_right,3)));
GenerateMap(histResultLeft, limit, pxAngle); 
title('Left Saccade cue')
box off
print('-dbmp','./Documents/Figures/Fig_2A_Left.bmp');
print('-depsc','./Documents/Figures/Fig_2A_Left.epsc');
GenerateMap(histResultRight, limit, pxAngle);
title('Right Saccade cue')
box off
print('-dbmp','./Documents/Figures/Fig_2A_Right.bmp');
print('-depsc','./Documents/Figures/Fig_2A_Right.epsc');

%% Figure 2B: Reaction Time Histogram
load('./Data/across_subjects.mat')

figure('Position', [900 350 700 500])
h = bar(BinsMsLatency(1:end-1), summary.across_subj_ms_latency);
set(h, 'FaceColor', 'b', 'EdgeColor', 'none')
hold on
stem(summary.ms_latency, 0.18, 'kv', 'LineWidth', 1.5)
xlabel('Microsaccade latency [ms]')
ylabel('Probability')
box off
set(gca,'YTick',0:.05:.2, 'XTick', 0:100:800, 'FontSize', 15)
xlim([190 810])

print('-djpeg','./Documents/Figures/Fig_2B.jpeg');
print('-depsc','./Documents/Figures/Fig_2B.epsc');

%% Figure 3: Distrubution of Horizontal Gaze (Cong., Incong., Neutral)
BinsHist = -30:30;
figure('Position', [900 350 400 500])
subplot(3,1,1)
h = bar(BinsHist(1:end - 1), summary.cong.dist_from_center);
set(h, 'FaceColor', 'b', 'EdgeColor', 'none')
hold on
plot([-20 -20], [0 .15], 'r--')
plot([20 20], [0 .15], 'r--');
plot([summary.cong.avg_dist_from_center, summary.cong.avg_dist_from_center], [0, 0.12], 'k','LineWidth',1.5)
box off
ylabel('Probability')
xlabel('Horizontal gaze position [arcmin]')
title('Congruent')
xlim([-30 30])
ylim([0, .15])
subplot(3,1,2)
h2 = bar(BinsHist(1:end - 1), summary.neutral.dist_from_center);
set(h2, 'FaceColor', 'g', 'EdgeColor', 'none')
hold on
plot([-20 -20], [0 .15], 'r--')
k = plot([20 20], [0 .15], 'r--');
plot([summary.neutral.avg_dist_from_center, summary.neutral.avg_dist_from_center], [0, 0.12], 'k','LineWidth',1.5)
box off
ylabel('Probability')
xlabel('Horizontal gaze position [arcmin]')
title('Neutral')
xlim([-30 30])
ylim([0, .15])
subplot(3,1,3)
h3 = bar(BinsHist(1:end - 1), summary.incong.dist_from_center);
set(h3, 'FaceColor', 'r', 'EdgeColor', 'none')
hold on
plot([-20 -20], [0 .15], 'r--')
plot([20 20], [0 .15], 'r--');
plot([summary.incong.avg_dist_from_center, summary.incong.avg_dist_from_center], [0, 0.12], 'k','LineWidth',1.5)
box off
ylabel('Probability')
xlabel('Horizontal gaze position [arcmin]')
title('Incongruent')
xlim([-30 30])
ylim([0, .15])
box off

print('-djpeg','./Documents/Figures/Fig_3.jpeg');
print('-depsc','./Documents/Figures/Fig_3.epsc');

%% Figure 4A: D-Prime Sensitivity
load('./Data/across_subjects.mat')
sem_dprime = [summary.cong.se_binned_dprime, summary.neutral.se_dprime, summary.incong.se_binned_dprime, summary.cong_no_ms.se_dprime, summary.incong_no_ms.se_dprime]/sqrt(6);
avg_dprime = [summary.cong.binned_dprime; summary.neutral.dprime; summary.incong.binned_dprime; summary.cong_no_ms.dprime; summary.incong_no_ms.dprime]';
errorbar(avg_dprime, sem_dprime, 'k', 'LineStyle', 'none', 'LineWidth', 2, ...
    'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', 'w')
set(gca, 'FontSize', 15,  'XTick', [ 1 2 3 4 5], 'XTickLabel', {'Cong.', 'Neutral', 'Incong.', 'Fix Cong.', 'Fix Incong.'})
xtickangle(45)
ylabel("Sensitivity (d')")
axis([0 6 0.5 2.5])
box off

print('-djpeg','./Documents/Figures/Fig_4A.jpeg');
print('-depsc','./Documents/Figures/Fig_4A.epsc');

%% Figure 4B: Response Times
load('./Data/across_subjects.mat')
figure('Position', [900 350 400 500]);
sem_resp = [summary.cong.se_binned_resp_time, summary.neutral.se_resp_time, summary.incong.se_binned_resp_time, summary.cong_no_ms.se_resp_time, summary.incong_no_ms.se_resp_time]/sqrt(6);
avg_resp = [summary.cong.binned_resp_time; summary.neutral.resp_time; summary.incong.binned_resp_time; summary.cong_no_ms.resp_time; summary.incong_no_ms.resp_time]';
errorbar(avg_resp, sem_resp, 'k', 'LineStyle', 'none', 'LineWidth', 2, ...
    'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', 'w')
set(gca, 'FontSize', 15,  'XTick', [ 1 2 3 4 5], 'XTickLabel', {'Cong.', 'Neutral', 'Incong.', 'Fix Cong.', 'Fix Incong.'})
xtickangle(45)
ylabel("Response Time [ms]")
axis([0 6 225 525])
box off
print('-djpeg','./Documents/Figures/Fig_4B.jpeg');
print('-depsc','./Documents/Figures/Fig_4B.epsc');

%% Figure 4C: Scatter of Congruent and Neutral Dprime Values
load('./Data/across_subjects.mat')
figure('Position', [900 300 400 500]);
hold on
scatter(indv.neutral.dprime, indv.cong.binned_dprime, 'o','MarkerFaceColor', rgb('grey'), 'MarkerEdgeColor', 'k', ...
    'SizeData', 100);
errorbar(summary.neutral.dprime, summary.cong.binned_dprime, summary.cong.se_binned_dprime/sqrt(6),...
    'k','LineStyle', '-','LineWidth', 1.5)
errorbar(summary.neutral.dprime, summary.cong.binned_dprime, summary.neutral.se_dprime/sqrt(6), ...
    'horizontal', 'k','LineStyle', '-','LineWidth', 1.5)
plot(summary.neutral.dprime, summary.cong.binned_dprime, ...
    'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 10, 'LineWidth', 1)
plot(0:3, 0:3, 'k-')
axis([.5, 3.5, .5, 3.5])
axis square
set(gca, 'FontSize', 15, 'XTick', 0:0.5:3.5, 'YTick', 0:0.5:3.5)
xlabel("Neutral sensitivity (d')")
ylabel("Congurent sensitivity (d')")
box off

print('-djpeg','./Documents/Figures/Fig_4C.jpeg');
print('-depsc','./Documents/Figures/Fig_4C.epsc');

%% Figure 5A: Sensitivity as a Function of Microsaccade Onset
load('./Data/across_subjects.mat')

figure('Position', [900 350 900 500])
errorbar(summary.temp_dynamics.ms_latency, summary.temp_dynamics.cong_incong_delta_dprime, summary.temp_dynamics.se_cong_incong_delta_dprime/sqrt(6),...
    'k','LineStyle', '-','LineWidth', 1.5)
hold on
errorbar(summary.temp_dynamics.ms_latency, summary.temp_dynamics.cong_incong_delta_dprime, summary.temp_dynamics.se_ms_latency/sqrt(6), ...
    'horizontal', 'k','LineStyle', '-','LineWidth', 1.5)
plot(summary.temp_dynamics.ms_latency, summary.temp_dynamics.cong_incong_delta_dprime, ...
    'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 8)
hn = plot([-1100 650],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime) mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)],'--k');
plot([-1100 650],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)+std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)...
    mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)+std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)],'-k')
plot([-1100 650],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)-std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)...
    mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)-std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)],'-k')
hb = plot(summary.temp_dynamics.ms_latency, summary.temp_dynamics.cong_neutral_delta_dprime, 'Color', [102/255, 0/255, 102/255], 'LineStyle', '-', 'LineWidth', 1.5);
hr = plot(summary.temp_dynamics.ms_latency, summary.temp_dynamics.incong_neutral_delta_dprime, 'Color', [2/255, 91/255, 82/255], 'LineStyle', '-', 'LineWidth', 1.5);
ht = plot([0 0], [-1 1.8], 'r', 'LineWidth', 2);
xlim([300 650])
ylim([-0.8 1.8])
set(gca, 'XTick',-1200:200:600, 'YTick', -.5:.5:2)
ylabel('\Delta sensitivity [d prime]')
xlabel('Time from saccade onset [ms]')
set(gca, 'FontSize', 15)
box off
title('Sensitivity as a function of microsaccade onset')
legend([ht, hb, hr, hn], {'Target onset','Cong. - neutral','Incong. - neutral','Fix. cong. dprime'})
print('-djpeg','./Documents/Figures/Fig_5A.jpeg');
print('-depsc','./Documents/Figures/Fig_5A.espc');

%% Supp Fig. 1: Distrubution of Horizontal Gaze (Left/Right Saccade Signal)
BinsHist = -30:30;
for ii = 1:6
figure('Position', [900 350 600 500])
saccHist{1} = indv.left_sacc_cue_ci.drift.x{ii};
saccHist{2} = indv.right_sacc_cue_ci.drift.x{ii};
MultipleHist(saccHist, 'binfactor', .25, 'samebins', 'smooth', 'color', 'winter', 'proportion')
hold on
plot([-20 -20], [0 .3], 'r--')
plot([20 20], [0 .3], 'r--');
box off
ylabel('Probability')
xlabel('Horizontal gaze position [arcmin]')
xlim([-30 30])
ylim([0, .27])
set(gca, 'FontSize', 15, 'YTick', 0:.05:.3)
print('-djpeg',sprintf('./Documents/Figures/SuppFig_1_%s.jpeg', subjects{ii}));
print('-depsc',sprintf('./Documents/Figures/SuppFig_1_%s.epsc', subjects{ii}));
end

%% Supp Fig. 2: Variability in Subject Landing Locations Heatmap
for ii = 1:6
load('./Data/across_subjects.mat')
pxAngle =  0.5278;
limit = 40;
figure;
newHeatmap = indv.gaze_heatmap_left(:,:,ii) + indv.gaze_heatmap_right(:,:,ii);
GenerateMap(newHeatmap, limit, pxAngle);
box off
axis off
print('-dbmp',sprintf('./Documents/Figures/SuppFig_2_%s.bmp', subjects{ii}));
print('-depsc',sprintf('./Documents/Figures/SuppFig_2_%s.epsc', subjects{ii}));
end

%% Supp Fig. 3: Individual Microsaccade Latencies
figure;
MultipleHist(indv.ms_latency_vector, 'binfactor', 1, 'samebins', 'smooth', 'color', 'parula')
print('-djpeg','./Documents/Figures/SuppFig_3.jpeg');
print('-depsc','./Documents/Figures/SuppFig_3.epsc');

%% Supp Fig. 4: Individual Dprime
subject_colors = parula(6);
figure('Position', [900 350 400 500]);
for jj = 1:length(indv.cong.dprime)
    hold on
    dprimeVector = [indv.cong.binned_dprime(jj), indv.neutral.dprime(jj), indv.incong.binned_dprime(jj), indv.cong_no_ms.dprime(jj), indv.incong_no_ms.dprime(jj)];
%     h1 = errorbar(dprimeVector, ci_dprime, 'color', subject_colors(jj,:), 'LineStyle', 'none', 'LineWidth', 2, ...
%         'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', subject_colors(jj,:));
    scatter(1:5, dprimeVector, 'o','MarkerFaceColor', subject_colors(jj,:), 'MarkerEdgeColor', 'none', ...
         'SizeData', 100);
     plot(1:5, dprimeVector, 'color', subject_colors(jj,:), 'LineWidth', 1.5)
end
set(gca, 'FontSize', 15,  'XTick', [ 1 2 3 4 5], 'XTickLabel', {'Cong.', 'Neutral', 'Incong.', 'Fix Cong.', 'Fix Incong.'})
xtickangle(45)
ylabel("Sensitivity (d')")
axis([0 6 0 3.75])
box off
print('-djpeg','./Documents/Figures/SuppFig_4.jpeg');
print('-depsc','./Documents/Figures/SuppFig_4.epsc');


%% Drift Coefficient
driftVal = [indv.cong.drift.coef; indv.neutral.drift.coef; indv.incong.drift.coef]';
[p.driftCoef, tbl.driftCoef, stats.driftCoef] = anova2(driftVal);
c.driftCoef = multcompare(stats.driftCoef);

figure('Position', [300, 300,400, 500]);
hold on
text(40,9.5, sprintf('Cong: %.2f arcmin^2/s', summary.cong.drift.coef))
text(40,9, sprintf('Neutral: %.2f arcmin^2/s', summary.neutral.drift.coef))
text(40,8.5, sprintf('Incong: %.2f arcmin^2/s', summary.incong.drift.coef))
timeInterval = 241; %255 is the max
hold on
shadedErrorBar(summary.cong.drift.timeDsq(1:timeInterval)*1000, summary.cong.drift.dsq(1:timeInterval), summary.cong.drift.se_dsq(1:timeInterval)/sqrt(6), 'k');
shadedErrorBar(summary.neutral.drift.timeDsq(1:timeInterval)*1000, summary.neutral.drift.dsq(1:timeInterval), summary.neutral.drift.se_dsq(1:timeInterval)/sqrt(6), {'Color', [0.7490, 0.2039, 0.0745]});
shadedErrorBar(summary.incong.drift.timeDsq(1:timeInterval)*1000, summary.incong.drift.dsq(1:timeInterval), summary.incong.drift.se_dsq(1:timeInterval)/sqrt(6), {'Color', [1, 0.7843, 0.0941]});
xlabel('Temporal interval [ms]')
ylabel('Displacement square [arcmin^2]')
set(gca, 'FontSize',12)
xlim([20 230])
box off
print('-djpeg','./Documents/Figures/DriftCoef.jpeg');
print('-depsc','./Documents/Figures/DriftCoef.epsc');

%% Drift Velocity
figure('Position', [300, 300,400, 500]);
errorbar([summary.cong.drift.speed, summary.neutral.drift.speed, summary.incong.drift.speed], ...
    [summary.cong.drift.se_speed, summary.neutral.drift.se_speed, summary.incong.drift.se_speed]/sqrt(6), ...
    'k', 'LineStyle', 'none', 'LineWidth', 2, ...
    'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', 'w')
ylabel('Velocity (armin/sec)')
set(gca, 'XTick', 1:3, 'XTickLabel', {'Cong','Neutral','Incong'}, 'FontSize', 12)
xtickangle(325)
xlim([0.5, 3.5])
box off
print('-djpeg','./Documents/Figures/DriftVel.jpeg');
print('-depsc','./Documents/Figures/DriftVel.epsc');

%% Single Subject Ms Rate Task vs Fixation
load('./Data/across_subjects.mat')
figure('position',[500, 300, 700, 500])
deltaMsRate = indv.ms_rate_fixation - indv.ms_rate_stimulus;
for ii = 1:length(indv.ms_rate_fixation)
   hold on
   plot(indv.ms_latency(ii), deltaMsRate(ii), 'Color', rgb('SeaGreen'),'Marker','.','MarkerSize',30)
end
axis([325, 675, 0, 1.8])
set(gca, 'YTick', 0:.3:2, 'FontSize', 15)
box off
ylabel('Fixation - task microsaccade rate [1/s]')
xlabel('Average microsaccade latency [ms]')
print('-djpeg','./Documents/Figures/SuppFig_2B.jpeg');
print('-depsc','./Documents/Figures/SuppFig_2B.epsc');

%% Landing Error 
figure('Position', [900 300 400 500]);
[t p b R] = LinRegression([indv.ms_fast_latency, indv.ms_slow_latency],indv.ms_landing_error,0,NaN, 1, 0)
axis([300 700 0 15])
xlabel('Microsaccade Latency')
ylabel('Landing Error')


figure('Position', [900 300 400 500]);
[t p b R] = LinRegression(indv.cong.binned_dprime - indv.incong.binned_dprime,indv.ms_landing_error,0,NaN, 1, 0)
axis([0 3 2 12])
xlabel('Delta Dprime')
ylabel('Landing Error')