close all; clear all; clc;

load('./Data/across_subjects.mat')
subject_colors = {'r' 'b' 'g' 'm' 'c' 'y'};
pxAngle =  0.5278;
limit = 40;
BinsHist = -30:30;
FontSize = 15;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Temporal Dynamics (Difference in sensititivy as a function of target onset

ms_latency_vector = -[summary.t4_t5_ms_latency, summary.t3_t4_ms_latency, summary.t2_t3_ms_latency, summary.t1_t2_ms_latency];
delta_dprime_vector = [summary.delta_t4_t5_dprime, summary.delta_t3_t4_dprime, summary.delta_t2_t3_dprime, summary.delta_t1_t2_dprime];
se_ms_latency_vector = [summary.se_t4_t5_ms_latency, summary.se_t3_t4_ms_latency, summary.se_t2_t3_ms_latency, summary.se_t1_t2_ms_latency];
se_delta_dprime_vector = [summary.se_delta_t4_t5_dprime, summary.se_delta_t3_t4_dprime, summary.se_delta_t2_t3_dprime, summary.se_delta_t1_t2_dprime];

figure('Position', [900 350 900 500])
errorbar(ms_latency_vector, delta_dprime_vector, se_delta_dprime_vector,...
    'k','LineStyle', '-','LineWidth',1)
hold on
errorbar(ms_latency_vector, delta_dprime_vector, se_ms_latency_vector, ...
    'horizontal', 'k','LineStyle', '-','LineWidth',1)
plot(ms_latency_vector, delta_dprime_vector, ...
    'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 8)
plot([-1100 650],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime) mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)],'--k')
plot([-1100 650],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)+std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)...
    mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)+std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)],'-k')
plot([-1100 650],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)-std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)...
    mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)-std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(6)],'-k')
plot([0 0], [-1 1.8], 'r', 'LineWidth', 2)
xlim([-50 500])
ylim([-0.5 1.8])
set(gca, 'XTick',-1200:200:600, 'YTick', -.5:.5:2)
ylabel('\Delta sensitivity [d prime]')
xlabel('Time from target onset [ms]')
set(gca, 'FontSize', 15)
box off
title('Sensitivity as a function of microsaccade onset')
print('-djpeg','./Documents/Figures/SensitivityTargetOnset.jpeg');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distance from center of display (orange histogram)

print('-djpeg','./Documents/Figures/DistFromCenter.jpeg');

figure('Position', [900 350 400 500])
h = bar(BinsHist(1:end - 1), summary.incong.dist_from_center);
set(h, 'FaceColor', 'b', 'EdgeColor', 'none')
hold on
h2 = bar(BinsHist(1:end - 1), summary.neutral.dist_from_center);
set(h2, 'FaceColor', 'g', 'EdgeColor', 'none')
h3 = bar(BinsHist(1:end - 1), summary.cong.dist_from_center);
set(h3, 'FaceColor', 'r', 'EdgeColor', 'none')

plot([-20 -20], [0 20], 'r--')
k = plot([20 20], [0 20], 'r--');
ylabel('Probability')
xlabel('Horizontal gaze position [arcmin]')
legend(k, 'Stimulus position')
legend boxoff
ylim([0 .14])
xlim([-30 30])
set(gca, 'FontSize', 15)
box off
title('Distance from center')
print('-djpeg','./Documents/Figures/DistFromCenterHist.jpeg');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fixation distribution 2D

GenerateMap(nanmean(indv.gaze_heatmap_cue, 3), limit, pxAngle); 
title('Directional trials')
caxis([.1 .9])
colorbar
GenerateMap(mean(indv.gaze_heatmap_neutral, 3), limit, pxAngle);
title('Neutral trials')
colorbar
caxis([.1 .9])
print('-djpeg','./Documents/Figures/FixationHeatmap.jpeg');


% FontSize = 15;
% figure
% [t p b R] = LinRegression(VarX,MS_RT,0,NaN, 1, 0)
% xlim([10 120]);
% ylim([200 800])
% xlabel('Hor. landing position variance [arcmin]')
% ylabel('Microsaccade reaction time [ms]')
% set(gca, 'FontSize', 10)
% 
% figure
% [t p b R] = LinRegression(Cong-Incong,MS_RT,0,NaN, 1, 0)
% xlim([10 120]);
% ylim([200 800])
% xlabel('Hor. landing position variance [arcmin]')
% ylabel('Microsaccade reaction time [ms]')
% set(gca, 'FontSize', 10)
% 
% figure
% [t p b R] = LinRegression(Cong-Incong,VarX,0,NaN, 1, 0)
% xlim([10 120]);
% ylim([200 800])
% xlabel('Hor. landing position variance [arcmin]')
% ylabel('Microsaccade reaction time [ms]')
% set(gca, 'FontSize', 10)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Microsaccade Landing Heatmap

GenerateMap(nanmean(indv.gaze_heatmap_left,3), limit, pxAngle); 
title('Left Saccade cue')
print('-djpeg','./Documents/Figures/TargetHeatmapLeft.jpeg');
GenerateMap(nanmean(indv.gaze_heatmap_right,3), limit, pxAngle);
title('Right Saccade cue')
print('-djpeg','./Documents/Figures/TargetHeatmapTight.jpeg');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D-Primes

[stat_test.anove.p.diff_trial_types, stat_test.anove.tbl.diff_trial_types, stat_test.anove.stats.diff_trial_types] = ...
    anova2([indv.cong.dprime; indv.neutral.dprime; indv.incong.dprime; indv.cong_no_ms.dprime; indv.incong_no_ms.dprime]');
[stat_test.mult_comp.c.diff_trial_types, ~, ~] = multcompare(stat_test.anove.stats.diff_trial_types);
[~, stat_test.ttest.cong_incong_no_ms] = ttest(indv.cong_no_ms.dprime, indv.incong_no_ms.dprime);
fprintf('mean d prime Cong: %.2f std: %.2f \n', summary.cong.dprime, summary.cong.se_dprime)
fprintf('mean d prime Neutral: %.2f std: %.2f \n', summary.neutral.dprime, summary.neutral.se_dprime)
fprintf('mean d prime Incong: %.2f std: %.2f \n', summary.incong.dprime, summary.incong.se_dprime)
fprintf('mean d prime Fix Cong: %.2f std: %.2f \n', summary.cong_no_ms.dprime, summary.cong_no_ms.se_dprime);
fprintf('mean d prime Fix Incong: %.2f std: %.2f \n', summary.incong_no_ms.dprime, summary.incong_no_ms.se_dprime);

ci_dprime = [tinv(0.975, length(indv.cong.dprime) - 1) * summary.cong.se_dprime, ...
    tinv(0.975, length(indv.neutral.dprime) - 1) * summary.neutral.se_dprime, ...
    tinv(0.975, length(indv.incong.dprime) - 1) * summary.incong.se_dprime, ...
    tinv(0.975, length(indv.cong_no_ms.dprime) - 1) * summary.cong_no_ms.se_dprime, ...
    tinv(0.975, length(indv.incong_no_ms.dprime) - 1) * summary.incong_no_ms.se_dprime];

sem_dprime = [summary.cong.se_dprime, ...
    summary.neutral.se_dprime,  ...
    summary.incong.se_dprime, ...
    summary.cong_no_ms.se_dprime, ...
    summary.incong_no_ms.se_dprime];

% figures (d prime)
figure('Position', [900 350 400 500]);
avg_dprime = [summary.cong.dprime; summary.neutral.dprime; summary.incong.dprime; summary.cong_no_ms.dprime; summary.incong_no_ms.dprime]';
errorbar(avg_dprime, sem_dprime, 'k','LineStyle', 'none','LineWidth',2)
hold on
plot(1:5, avg_dprime, 'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 12)
% for jj = 1:length(Cong)
%     plot([1:5],v(jj,:), 'o','MarkerFacesubject_colorsor', subject_colors{jj}, 'MarkerEdgesubject_colorsor', 'none');
% end
set(gca, 'FontSize', FontSize, ...
   'XTick', [ 1 2 3 4 5], 'XTickLabel', {'Cong.', 'Neutral', 'Incong.', 'Fix Cong.', 'Fix Incong.'})
xtickangle(45)
ylabel("Sensitivity (d')")
ylim([0 3])
xlim([0 6])
box off
print('-djpeg','./Documents/Figures/DPrime.jpeg');

[stat_test.anova.p.dist_from_target, stat_test.anova.tbl.dist_from_target, stat_test.anova.stats.dist_from_target] = ...
    anova2([indv.cong.dist_from_target; indv.neutral.dist_from_target; indv.incong.dist_from_target; ...
    indv.cong_no_ms.dist_from_target; indv.incong_no_ms.dist_from_target]');
[stat_test.mult_comp.c.dist_from_target,~,~] = multcompare(stat_test.anova.stats.dist_from_target);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Response Times

figure('Position', [900 350 400 500]);
avg_response_time = [summary.cong.avg_resp_time; summary.neutral.avg_resp_time; summary.incong.avg_resp_time; ...
    summary.cong_no_ms.avg_resp_time; summary.incong_no_ms.avg_resp_time]';
sem_response_time = [summary.cong.se_avg_resp_time, ...
    summary.neutral.se_avg_resp_time, ...
    summary.incong.se_avg_resp_time, ...
    summary.cong_no_ms.se_avg_resp_time, ...
    summary.incong_no_ms.se_avg_resp_time];
errorbar(avg_response_time, sem_response_time, 'k', 'LineStyle', 'none', 'LineWidth', 2)
hold on
plot(1:5, avg_response_time, 'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 12)
% for jj = 1:length(Cong)
%     plot([1:5],v(jj,:), 'o','MarkerFacesubject_colorsor', subject_colors{jj}, 'MarkerEdgesubject_colorsor', 'none');
% end
set(gca, 'FontSize', FontSize, ...
   'XTick', [1:5], 'XTickLabel', {'Cong.', 'Neutral', 'Incong.', 'Fix Cong.', 'Fix Incong.'})
xtickangle(45)
ylabel('Response Time [ms]')
xlim([0 6])
box off
print('-djpeg','./Documents/Figures/ResponseTime.jpeg');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distance from target

figure('Position', [900 350 400 500]);
avg_dist_from_target = [summary.cong.dist_from_target; summary.neutral.dist_from_target; summary.incong.dist_from_target; ...
    summary.cong_no_ms.dist_from_target; summary.incong_no_ms.dist_from_target]';
sem_dist_from_target = [summary.cong.se_dist_from_target, ...
    summary.neutral.se_dist_from_target, ...
    summary.incong.se_dist_from_target, ...
    summary.cong_no_ms.se_dist_from_target, ...
    summary.incong_no_ms.se_dist_from_target];

plot(1:5, avg_dist_from_target, 'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 12)
hold on
errorbar(avg_dist_from_target, sem_dist_from_target, 'k', 'LineStyle', 'none', 'LineWidth', 2)
% for jj = 1:length(Cong)
%     plot([1:5],v(jj,:), 'o','MarkerFacesubject_colorsor', subject_colors{jj}, 'MarkerEdgesubject_colorsor', 'none');
% end
set(gca, 'FontSize', FontSize, ...
   'XTick', [1:5], 'XTickLabel', {'Cong.', 'Neutral', 'Incong.', 'Fix Cong.', 'Fix Incong.'})
xtickangle(45)
ylabel('Distance From Target (arcmin)')
ylim([19 22])
xlim([0 6])
box off
print('-djpeg','./Documents/Figures/DistFromTarget.jpeg');


[stat_test.anova.p.dist_from_target, stat_test.anova.tbl.dist_from_target, stat_test.anova.stats.dist_from_target] = ...
    anova2([indv.cong.dist_from_target; indv.neutral.dist_from_target; indv.incong.dist_from_target]');
[stat_test.mult_comp.c.dist_from_target, ~, ~] = multcompare(stat_test.anova.stats.dist_from_target);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reaction Time of the 1st Ms after Saccade Signal

figure('Position', [900 350 700 500])
MultipleHist(hist_struct, 'binfactor',1, 'samebins','smooth')
set(gca, 'FontSize', FontSize)
xlabel('Latency [ms]')
ylabel('Probability')
box off

figure('Position', [900 350 400 500]);
avg_ms_latency = [summary.cong.median_ms_latency; summary.incong.median_ms_latency]';
sem_ms_latency = [summary.cong.se_median_ms_latency, ...
    summary.incong.se_median_ms_latency];

errorbar(avg_ms_latency, sem_ms_latency, 'k', 'LineStyle', 'none', 'LineWidth', 2)
hold on
plot(1:2, avg_ms_latency, 'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 12)
% for jj = 1:length(indv.cong.dprime) % For these, need individual subject 
%     plot(1:2,avg_ms_latency(jj,:), 'o','MarkerFaceColor', subject_colors{jj}, 'MarkerFaceColor', 'none');
% end
set(gca, 'FontSize', FontSize, ...
   'XTick', [ 1 2 ], 'XTickLabel', {'Cong.', 'Incong.'})
xtickangle(45)
ylabel('Microsaccade Reaction Times (ms)')
ylim([350 550])
xlim([0 3])
box off
print('-djpeg','./Documents/Figures/ReactionTime.jpeg');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bias in the different conditions

figure('Position', [900 350 400 500]);
avg_crit = ([summary.cong.crit_dprime; summary.neutral.crit_dprime; summary.incong.crit_dprime; ...
    summary.cong_no_ms.crit_dprime; summary.incong_no_ms.crit_dprime])';
sem_crit = [summary.cong.se_crit_dprime, ...
    summary.neutral.se_crit_dprime, ...
    summary.incong.se_crit_dprime, ...
    summary.cong_no_ms.se_crit_dprime, ...
    summary.incong_no_ms.se_crit_dprime];

plot(1:5, avg_crit, 'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 12)
hold on
plot([0 6], [0 0], 'r--')
errorbar(avg_crit, sem_crit, 'k','LineStyle', 'none','LineWidth',2)
% for jj = 1:length(Cong)
%     plot([1:5],v(jj,:), 'o','MarkerFacesubject_colorsor', subject_colors{jj}, 'MarkerEdgesubject_colorsor', 'none');
% end
set(gca, 'FontSize', FontSize, ...
   'XTick', [ 1 2 3 4 5], 'XTickLabel', {'Cong.', 'Neutral', 'Incong.', 'Fix Cong.', 'Fix Incong.'})
xtickangle(45)
ylabel('Bias')
ylim([-1 1])
xlim([0 6])
box off
print('-djpeg','./Documents/Figures/Bias.jpeg');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of counts

counter_table = table(counter(:,1),counter(:,2),counter(:,3),counter(:,4),counter(:,5),counter(:,6), ...
    'VariableNames', {'Adriana','Anne','Cody','Kamshat','Karina','Melissa'}, ...
    'RowNames', {'cong','incong','neutral','cong_no_ms','incong_no_ms','neutral_cong','neutral_incong','cong_invalid','incong_valid'});
writetable(counter_table,'counter_table.dat','WriteRowNames',true, 'Delimiter',' ')  
type 'counter_table.dat'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Linear Regressions

figure('Position',[200, 300, 1000, 500])
subplot(1,2,1)
[t p b R] = LinRegression(indv.cong.landing_error,indv.cong.performance,0, NaN, 1, 0);
axis([min(indv.cong.landing_error)-.5 max(indv.cong.landing_error)+.5 .5 1])
set(gca, 'FontSize',12, 'XTick',(4:1:9),'YTick',(.5:.1:1))
xlabel('Average Landing Error')
ylabel('Proportion Correct')
title({'Congruent'; sprintf('r = %.2f, p = %.2f b = %.2f', R, p, b)})
subplot(1,2,2)
[t p b R] = LinRegression(indv.incong.landing_error,indv.incong.performance,0, NaN, 1, 0);
axis([min(indv.incong.landing_error)-.5 max(indv.incong.landing_error)+.5 .5 1])
set(gca, 'FontSize',12, 'XTick',(36:1:42),'YTick',(.5:.1:1))
xlabel('Average Landing Error')
ylabel('Proportion Correct')
title({'Incongruent'; sprintf('r = %.2f, p = %.2f b = %.2f', R, p, b)})
print('-djpeg','./Documents/Figures/PerformanceRegressions.jpeg');

[t p b R] = LinRegression(indv.cong.median_ms_latency,indv.cong.performance,0, NaN, 1, 0);
axis([min(indv.cong.median_ms_latency)-.5 max(indv.cong.median_ms_latency)+.5 .5 1])
[t p b R] = LinRegression(indv.incong.median_ms_latency,indv.incong.performance,0, NaN, 1, 0);
axis([min(indv.incong.median_ms_latency)-.5 max(indv.incong.median_ms_latency)+.5 .5 1])


landingErrorLeftX = cellfun(@nanmean, indv.ms_landing_error_left_x);
landingErrorLeftY = cellfun(@nanmean, indv.ms_landing_error_left_y);

landingErrorRightX = cellfun(@nanmean, indv.ms_landing_error_right_x);
landingErrorRightY = cellfun(@nanmean, indv.ms_landing_error_right_y);

figure
[t p b R] = LinRegression(indv.ms_rate_fixation, landingErrorRightY,0, NaN, 1, 0);
axis([1 4 -6 10])
