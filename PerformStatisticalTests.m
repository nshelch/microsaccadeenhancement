% Fixation Rate for Slow and Fast Latency
[~, statTest.msFixRateSlowFast] = ttest2(indv.ms_fast_fix_rate, indv.ms_slow_fix_rate);

% Dprime Stat Test
dprimeAnova = [indv.cong.binned_dprime; indv.neutral.dprime; indv.incong.binned_dprime; indv.cong_no_ms.dprime; indv.incong_no_ms.dprime]';
[statTest.anova.p.dprime, statTest.anova.tbl.dprime, statTest.anova.stats.dprime] = ...
    anova2(dprimeAnova);
[statTest.mult_comp.c.dprime,~,~] = multcompare(statTest.anova.stats.dprime);

% Dprime Temp Dynamics Stat Test
[statTest.anova.p.tempDynamics, statTest.anova.tbl.tempDynamics, statTest.anova.stats.tempDynamics] = ...
    anova2(indv.temp_dynamics.cong_incong_delta_dprime);
[statTest.mult_comp.c.tempDynamics,~,~] = multcompare(statTest.anova.stats.tempDynamics);

% Response Time Stat Test
respTimeAnova = [indv.cong.binned_resp_time; indv.neutral.resp_time; indv.incong.binned_resp_time; indv.cong_no_ms.resp_time; indv.incong_no_ms.resp_time]';
[statTest.anova.p.resp_time, statTest.anova.tbl.resp_time, statTest.anova.stats.resp_time] = ...
    anova2(respTimeAnova);
[statTest.mult_comp.c.resp_time,~,~] = multcompare(statTest.anova.stats.resp_time);

% Dist from center during target presentation
[~, statTest.ttest.distCongNeutral] = ttest(indv.cong.avg_dist_from_center, indv.neutral.avg_dist_from_center);
[~, statTest.ttest.distCongIncong] = ttest(indv.cong.avg_dist_from_center, indv.incong.avg_dist_from_center);
[~, statTest.ttest.distIncongNeutral] = ttest(indv.incong.avg_dist_from_center, indv.neutral.avg_dist_from_center);
[~, statTest.ttest.saccCueLeftRight] = ttest(indv.left_sacc_cue_ci.avg_dist_from_center, indv.right_sacc_cue_ci.avg_dist_from_center);

[~, statTest.ttest.fixCong_fixIncong] = ttest(indv.cong_no_ms.dprime, indv.incong_no_ms.dprime);
[~, statTest.ttest.fixCong_Neutral] = ttest(indv.cong_no_ms.dprime, indv.neutral.dprime);
[~, statTest.ttest.fixIncong_Neutral] = ttest(indv.incong_no_ms.dprime, indv.neutral.dprime);
[~, statTest.ttest.tempDynamics] = ttest(indv.temp_dynamics.cong_incong_delta_dprime(:,1), indv.temp_dynamics.cong_incong_delta_dprime(:,end));
