close all; clear all; clc;

Subject = {'Adriana','Anne','Cody','Kamshat','Karina','Melissa'};

for sub_idx = 1:length(Subject)
    
    load(sprintf('./Data/%s.mat', Subject{sub_idx}))
    
    MaxMSaccAmp = 90;
    [vt] = preprocessing(data,MaxMSaccAmp);
    [pxAngle] = CalculatePxAngle(vt{1}.Xres, 1230, 390);
    vt = osRemoverInEM(vt);
    
    if strcmp(Subject{sub_idx}, 'Melissa')
        DelayAfterTOn = 550; % time delay (ms) after the target is on to consider a micorsaccade in the analysis
        Dist_thresh = 15; % arcmin distance from center of display in the period preceding the target appearence
        DistTarget_tresL = 0; % arcmin distance from the target  lower bound
        DistTarget_tresU = 35; % arcmin distance from the target upper bound
        MaxMsAmp = 60;
        MinMsAmp = 0;
    elseif strcmp(Subject{sub_idx}, 'Kamshat')
        DelayAfterTOn = 550;
        Dist_thresh = 10;
        DistTarget_tresL = 15;
        DistTarget_tresU = 25;
        MaxMsAmp = 30;
        MinMsAmp = 0;
    else
        DelayAfterTOn = 450;
        Dist_thresh = 10;
        DistTarget_tresL = 15;
        DistTarget_tresU = 25;
        MaxMsAmp = 30;
        MinMsAmp = 0;
    end
    
    % ms counters
    cont_ms = 0;
    cont_early = 0;
    cont_late = 0;
    cont_c = 0;
    cont_i = 0;
    cont_n = 0;
    cont_noms_incong = 0;
    cont_noms_cong = 0;
    cont_v_early_cong = 0;
    cont_v_early_incong = 0;
    
    for ii = 1:length(vt)
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Saves cued target orientation
        if vt{ii}.CueLocation == 1
            if vt{ii}.Target1Orientation == 135
                orientation_cued_target(ii) = 0;
            elseif vt{ii}.Target1Orientation == 45
                orientation_cued_target(ii) =  1;
            end
        end
        if vt{ii}.CueLocation == 2
            if vt{ii}.Target2Orientation == 135
                orientation_cued_target(ii) = 0;
            elseif vt{ii}.Target2Orientation == 45
                orientation_cued_target(ii) =  1;
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Finds all microsaccades that occured after saccade cue signal
        sacc_cue_on = vt{ii}.TimeFixationON + vt{ii}.FixationTime + vt{ii}.BeepDelayTime;
        idx_valid_ms = find(vt{ii}.microsaccades.start > sacc_cue_on);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % eliminate trials with no tracks, blinks, large saccades, no response,
        % and too short trial duration
        [output] = filter_trial(vt{ii}, []);
        if output
            
            % eliminates microsaccades that occurred at the very beginning
            % of the trial and were used to recenter the gaze
            id = find(vt{ii}.microsaccades.start > 50);
            ms_start = vt{ii}.microsaccades.start(id);
            ms_end = vt{ii}.microsaccades.duration(id) + ms_start;
            ms_amp = vt{ii}.microsaccades.amplitude(id);
            
            % find the microsaccades/drifts performed 80 - 300 ms from target on
            % these two numbers can be parameters at the beginning of the code script
            target_on = vt{ii}.TimeTargetON;
            idx_valid_ms = find( ms_start > (target_on + 80)  & ms_start < (target_on + DelayAfterTOn) ); % ms occured during period of interest
            v_early_ms = find(ms_start > 50 & ms_start <= target_on);
            idx_no_ms = find( (ms_start > (target_on - 100) & ms_start < vt{ii}.TimeCueON), 1 ); % no ms occuring during slightly wider period of interest

            % make sure there are no ms or saccade in the preceding period
            id_ms_clear = find( ms_start > (sacc_cue_on - 80) & ms_start < target_on, 1 ); % ms occuring prior to period of interest
            
            % check that the average gaze position when the target is on is
            % close to 0
            xx_target = vt{ii}.x.position(round(target_on - 50):round(target_on + vt{ii}.TargetTime)) + vt{ii}.xoffset * pxAngle;
            yy_target = vt{ii}.y.position(round(target_on - 50):round(target_on + vt{ii}.TargetTime)) + vt{ii}.yoffset * pxAngle;
            % gaze location
            gaze_loc = mean(sqrt(xx_target.^2 + yy_target.^2));
            
            % distance from the cued location
            if vt{ii}.CueLocation == 2
                dist_from_target = sqrt((xx_target - (-double(vt{ii}.TargetOffsetpx) * pxAngle)).^2  + (yy_target.^2 ));
            elseif vt{ii}.CueLocation == 1
                dist_from_target = sqrt((xx_target - (double(vt{ii}.TargetOffsetpx) * pxAngle)).^2 + (yy_target.^2 )) ;
            end
            
            % Checks for clean trials
            if isempty(id_ms_clear) && ...
                    mean(gaze_loc) < Dist_thresh &&...
                    mean(dist_from_target) > DistTarget_tresL && ...
                    mean(dist_from_target) < DistTarget_tresU
                
                % Makes sure a microsaccade was performed at some point
                if ~isempty(idx_valid_ms) && ...
                        ms_amp(idx_valid_ms(1)) < MaxMsAmp && ...
                        ms_amp(idx_valid_ms(1)) > MinMsAmp
                    
                    % Trials in which a microsaccade was performed
                    ms_idx = idx_valid_ms(1);
                    
                    ft.ms_end_xpos(ii) = vt{ii}.x.position(ms_end(ms_idx(1)));
                    ft.ms_end_ypos(ii) = vt{ii}.y.position(ms_end(ms_idx(1)));
                    
                    % CONGRUENT TRIALS: ms = target
                    if ( (ft.ms_end_xpos(end) < 0 && vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 2) || ...
                            (ft.ms_end_xpos(end) > 0 && vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 1) )
                        
                        cont_c = cont_c + 1;
                        cong.perf(cont_c) = vt{ii}.Correct;
                        cong.id(cont_c) = ii;
                        cong.ms_latency(cont_c) = target_on - ms_start(ms_idx);
                        
                        % INCONGRUENT TRIALS: ms != target
                    elseif ( (ft.ms_end_xpos(end) < 0 && vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 2) || ...
                            (ft.ms_end_xpos(end) > 0 && vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 1) )
                        
                        cont_i = cont_i + 1;
                        incong.perf(cont_i) = vt{ii}.Correct;
                        incong.id(cont_i) = ii;
                        incong.ms_latency(cont_i) = target_on - ms_start(ms_idx);
                        
                    end
                elseif isempty(idx_no_ms)
                    
                    %                 tmp_drift.x = vt{ii}.x.position(drift_start(idx_valid_drift):drift_end(idx_valid_drift));
                    %                 tmp_drift.y = vt{ii}.y.position(drift_start(idx_valid_drift):drift_end(idx_valid_drift));
                    %
                    %                 % Calculate velocity and check if its less than
                    %                 % MaxVelocity filter
                    %                 [~,Velocity] = CalculateDriftVelocity(tmp_drift, 1);
                    %                 vel_idx = find(Velocity <= MaxVelocity, 1);
                    %
                    %                 % Calculated diffusion coef to check against
                    %                 % MaxDiffCoef filter
                    %                 [~,~,~,~,~,ind_seg,~,~] = CalculateDiffusionCoef(tmp_drift);
                    %
                    
                    % CONGRUENT TRIALS:
                    if ( (vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 1) ||...
                            (vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 2) )
                        
                        cont_noms_cong = cont_noms_cong + 1;
                        cong_no_ms.perf(cont_noms_cong) = vt{ii}.Correct;
                        cong_no_ms.id(cont_noms_cong) = ii;
                        cong_no_ms.gaze_pos(cont_noms_cong) = gaze_loc;
                        cong_no_ms.dist_from_target{cont_noms_cong} = dist_from_target;
                        %                     if ~isempty(idx_valid_drift) && ...
                        %                             ~isempty(vel_idx) && ...
                        %                             ind_seg(end) <= MaxDiffCoef
                        %                         cont_nodr_cong = cont_nodr_cong + 1;
                        %                         cong_no_ms.drift_ind_vel{cont_nodr_cong} = Velocity;
                        %                         cong_no_ms.drift_pos(cont_nodr_cong) = tmp_drift;
                        % %                         cong_no_ms.drift_count{cont_nodr_cong} = [ii, drift_start(idx_valid_drift)];
                        %                     end
                        
                        % NEUTRAL TRIALS
                    elseif vt{ii}.SaccCueType == 0
                        
                        cont_n = cont_n + 1;
                        neutral.perf(cont_n) = vt{ii}.Correct;
                        neutral.id(cont_n) = ii;
                        neutral.gaze_pos(cont_n) = gaze_loc;
                        neutral.dist_from_target{cont_n} = dist_from_target;
                        %                         neutral.gaze_heatmap_xx = [neutral.gaze_heatmap_xx; vt{ii}.x.position(gaze_heatmap_time) + vt{ii}.xoffset * pxAngle];
                        %                         neutral.gaze_heatmap_yy = [neutral.gaze_heatmap_yy; vt{ii}.y.position(gaze_heatmap_time) + vt{ii}.xoffset * pxAngle];
                        %                     if ~isempty(idx_valid_drift) && ...
                        %                             ~isempty(vel_idx) && ...
                        %                             ind_seg(end) <= MaxDiffCoef
                        %                         cont_n_dr = cont_n_dr + 1;
                        %                         neutral.drift_ind_vel{cont_n_dr} = Velocity;
                        %                         neutral.drift_pos(cont_n_dr) = tmp_drift;
                        % %                         neutral.drift_count{cont_n_dr} = [ii, drift_start(idx_valid_drift)];
                        %                     end
                        
                        % INCONGRUENT TRIALS
                    elseif((vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 2) ||...
                            (vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 1) )
                        
                        cont_noms_incong = cont_noms_incong + 1;
                        incong_no_ms.perf(cont_noms_incong) = vt{ii}.Correct;
                        incong_no_ms.id(cont_noms_incong) = ii;
                        incong_no_ms.gaze_pos(cont_noms_incong) = gaze_loc;
                        incong_no_ms.dist_from_target{cont_noms_incong} = dist_from_target;
                        %                     if ~isempty(idx_valid_drift) && ...
                        %                             ~isempty(vel_idx) && ...
                        %                             ind_seg(end) <= MaxDiffCoef
                        %                         cont_nodr_incong = cont_nodr_incong + 1;
                        %                         incong_no_ms.drift_ind_vel{cont_nodr_incong} = Velocity;
                        %                         incong_no_ms.drift_pos(cont_nodr_incong) = tmp_drift;
                        % %                         incong_no_ms.drift_count{cont_nodr_incong} = [ii, drift_start(idx_valid_drift)];
                        %                     end
                    end
                elseif ~isempty(v_early_ms) && isempty(idx_valid_ms)
                    % Trials in which a microsaccade was performed
                    cont_early = cont_early + 1;
                    ms_idx = v_early_ms(1);
                    
                    ft.ms_end_xpos(ii) = vt{ii}.x.position(ms_end(ms_idx(1)));
                    ft.ms_end_ypos(ii) = vt{ii}.y.position(ms_end(ms_idx(1)));
                    
                    % CONGRUENT TRIALS: ms = target
                    if ( (ft.ms_end_xpos(end) < 0 && vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 2) || ...
                            (ft.ms_end_xpos(end) > 0 && vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 1) )
                        
                        cont_v_early_cong = cont_v_early_cong + 1;
                        cong.v_early_ms_perf(cont_v_early_cong) = vt{ii}.Correct;
                        cong.v_early_ms_id(cont_v_early_cong) = ii;
                        cong.v_early_ms_latency(cont_v_early_cong) = target_on - ms_start(ms_idx);
                        
                        % INCONGRUENT TRIALS: ms != target
                    elseif ( (ft.ms_end_xpos(end) < 0 && vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 2) || ...
                            (ft.ms_end_xpos(end) > 0 && vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 1) )
                        
                        cont_v_early_incong = cont_v_early_incong + 1;
                        incong.v_early_ms_perf(cont_v_early_incong) = vt{ii}.Correct;
                        incong.v_early_ms_id(cont_v_early_incong) = ii;
                        incong.v_early_ms_latency(cont_v_early_incong) = target_on - ms_start(ms_idx);
                        
                    end
                    
                end
            end
        end
    end
    
    subj_resp = cellfun(@(z) z(:).Response, vt);
    sacc_cue_type = cellfun(@(z) z(:).SaccCueType, vt);
    cue_location = cellfun(@(z) z(:).CueLocation, vt);
    
    ft.orientation_cued_target = orientation_cued_target;
    ft.sacc_cue_type = sacc_cue_type;
    ft.cue_location = cue_location;
    ft.subj_resp = subj_resp;
    ft.cong = cong;
    ft.incong = incong;
    ft.incong_no_ms = incong_no_ms;
    ft.cong_no_ms = cong_no_ms;
    ft.neutral = neutral;
    
    trial_types = {'cong','incong','neutral','incong_no_ms','cong_no_ms'};
    
    for t_idx = 1:length(trial_types)
        trial = trial_types{t_idx};
        subj_resp = ft.subj_resp(ft.(trial).id);
        target_orientation = ft.orientation_cued_target(ft.(trial).id);
        
        if strcmp(trial, 'cong') || strcmp(trial,'incong')
            [~, ~, sens_thresh, ~] = PartitionEqualGroups(ft.(trial).ms_latency', 4);
            
            for thresh_idx = 1:length(sens_thresh) - 1
                sens_bins = sprintf('t%d_t%d_idx', thresh_idx, thresh_idx + 1);
                indv.(trial).(sens_bins) = find(ft.(trial).ms_latency > sens_thresh(thresh_idx) & ft.(trial).ms_latency <= sens_thresh(thresh_idx + 1));
                indv.(trial).(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(sub_idx) = CalculateDprime_2(subj_resp(indv.(trial).(sens_bins)), target_orientation(indv.(trial).(sens_bins)));
            end
            indv.(trial).v_early_dprime(sub_idx) = CalculateDprime_2(ft.subj_resp(ft.(trial).v_early_ms_id), ft.orientation_cued_target(ft.(trial).v_early_ms_id));
            sprintf('Number of points: %d', length(ft.cong.v_early_ms_id) + length(ft.incong.v_early_ms_id))
            input ''
        end
        
        indv.(trial).dprime(sub_idx) = CalculateDprime_2(ft.subj_resp(ft.(trial).id), ft.orientation_cued_target(ft.(trial).id));
        
    end
    
    
    for thresh_idx = 1:length(sens_thresh) - 1
        indv.(sprintf('t%d_t%d_ms_latency', thresh_idx, thresh_idx + 1))(sub_idx) = ...
            mean([ft.cong.ms_latency(indv.cong.(sprintf('t%d_t%d_idx', thresh_idx, thresh_idx + 1))), ...
            ft.incong.ms_latency(indv.incong.(sprintf('t%d_t%d_idx', thresh_idx, thresh_idx + 1)))]);
        
        indv.(sprintf('delta_t%d_t%d_dprime', thresh_idx, thresh_idx + 1))(sub_idx) = ...
            indv.cong.(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(sub_idx) - ...
            indv.incong.(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(sub_idx);
    end
    
    indv.v_early_ms_latency(sub_idx) = mean([ft.cong.v_early_ms_latency, ft.incong.v_early_ms_latency]);
    indv.delta_v_early_dprime(sub_idx) = indv.cong.v_early_dprime(sub_idx) - indv.incong.v_early_dprime(sub_idx);
    
    clc;
    
    save(sprintf('./Data/sens_%s.mat', Subject{sub_idx}), 'ft','cong','incong')
    clearvars -except sub_idx Subject indv
    
end

trial_types = {'cong','incong','neutral','incong_no_ms','cong_no_ms'};

for t_idx = 1:length(trial_types)
    trial = trial_types{t_idx};
    var_names = fieldnames(indv.(trial));
    for var_idx = 1:length(var_names)
        cur_var = var_names{var_idx};
        summary.(trial).(cur_var) = nanmean(indv.(trial).(cur_var));
        summary.(trial).(sprintf('se_%s', cur_var)) = nansem(indv.(trial).(cur_var));
    end
end

var_names = fieldnames(indv);
for ii = 6:length(var_names)
    cur_var = var_names{ii};
    summary.(cur_var) = nanmean(indv.(cur_var));
    summary.(sprintf('se_%s',cur_var)) = nansem(indv.(cur_var));
end

ms_latency_vector = [summary.v_early_ms_latency, summary.t4_t5_ms_latency, summary.t3_t4_ms_latency, summary.t2_t3_ms_latency, summary.t1_t2_ms_latency];
delta_dprime_vector = [summary.delta_v_early_dprime, summary.delta_t4_t5_dprime, summary.delta_t3_t4_dprime, summary.delta_t2_t3_dprime, summary.delta_t1_t2_dprime];
se_ms_latency_vector = [summary.se_v_early_ms_latency, summary.se_t4_t5_ms_latency, summary.se_t3_t4_ms_latency, summary.se_t2_t3_ms_latency, summary.se_t1_t2_ms_latency];
se_delta_dprime_vector = [summary.se_delta_v_early_dprime, summary.se_delta_t4_t5_dprime, summary.se_delta_t3_t4_dprime, summary.se_delta_t2_t3_dprime, summary.se_delta_t1_t2_dprime];

% Temporal Dynamics (Difference in sensititivy as a function of target onset
figure('Position', [900 350 900 500])
errorbar(ms_latency_vector, delta_dprime_vector, se_delta_dprime_vector,...
    'k','LineStyle', '-','LineWidth',1)
hold on
errorbar(ms_latency_vector, delta_dprime_vector, se_ms_latency_vector, ...
    'horizontal', 'k','LineStyle', '-','LineWidth',1)
plot(ms_latency_vector, delta_dprime_vector, ...
    'ok', 'MarkerFaceColor', 'w', 'MarkerSize', 8)
plot([0 0], [-1 1.8], 'r', 'LineWidth', 2)
xlim([-650 1100])
ylim([-0.5 1.8])
plot([-550 1000],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime) mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)],'--k')
plot([-550 1000],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)+std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(ii)...
    mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)+std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(ii)],'-k')
plot([-550 1000],...
    [mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)-std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(ii)...
    mean(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)-std(indv.cong_no_ms.dprime-indv.incong_no_ms.dprime)/sqrt(ii)],'-k')
set(gca, 'XTick',-400:200:1200, 'YTick', -.5:.5:2, 'Xdir', 'reverse' )
ylabel('\Delta sensitivity [d prime]')
xlabel('Time from target onset [ms]')
set(gca, 'FontSize', 15)
box off
title('Sensitivity as a function of target onset')
print('-djpeg','./Documents/Figures/SensitivityTargetOnset.jpeg');

save('./Data/sensitivity_values.mat','indv','summary')
