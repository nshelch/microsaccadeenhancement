function [output, dt] = filter_trial(trial, dt)

if check_blinks(trial)
    dt.blinks = dt.blinks + 1;
    output = 0;
elseif check_no_tracks(trial)
    dt.no_track = dt.no_track + 1;
    output = 0;
elseif ~check_trial_duration(trial) % returns 0 if trial duration is short
    dt.too_short = dt.too_short + 1;
    output = 0;
elseif ~manual_discard(trial) % returns 1 if vt.invalid is empty
    dt.manual_discard = dt.manual_discard + 1;
    output = 0;
elseif check_saccades(trial)
    dt.saccades = dt.saccades + 1;
    output = 0;
elseif check_no_response(trial)
    dt.no_response = dt.no_response + 1;
    output = 0;
else
    output = 1;
end

end

function output = check_blinks(trial)
fix_on = (trial.TimeFixationON + trial.FixationTime + trial.BeepDelayTime) - 50;
target_on = trial.TimeTargetON;
output = isIntersectedIn( (fix_on - 100), (target_on + 400) - (fix_on - 100), trial.blinks);
end

function output = check_no_tracks(trial)
fix_on = (trial.TimeFixationON + trial.FixationTime + trial.BeepDelayTime) - 50;
target_on = trial.TimeTargetON;
output = isIntersectedIn( (fix_on - 100), (target_on + 400) - (fix_on - 100), trial.notracks);
end

function output = check_saccades(trial)
fix_on = (trial.TimeFixationON + trial.FixationTime + trial.BeepDelayTime) - 50;
target_on = trial.TimeCueON;
output = isIntersectedIn(fix_on, target_on - fix_on, trial.saccades);
end

function output = check_trial_duration(trial)
output = trial.TimeCueON < length(trial.x.position);
end

function output = check_no_response(trial)
output = trial.Correct > 2;
end

function output = manual_discard(trial)
output = isempty(trial.invalid.start);
end