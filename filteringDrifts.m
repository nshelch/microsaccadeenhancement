
Subject = {'Adriana','Anne','Cody','Kamshat','Karina','Melissa'};

for sub_idx = 1:length(Subject)
    
    load(sprintf('./Data/%s.mat', Subject{sub_idx}))
    load(sprintf('./Data/%s_summary.mat', Subject{sub_idx}))
    fprintf('Subject: %s\n\n', Subject{sub_idx})
    
    for ii = 1:length(vt)
       vt{ii}.pxAngle = CalculatePxAngle(vt{ii}.Xres, 1230, 390); 
    end
    trial_types = {'cong','incong','neutral','cong_no_ms','incong_no_ms'};
    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        fprintf('Subject: %s \t Trial Type: %s\n', Subject{sub_idx}, cur_trial)
        
        vt = checkDrifts(ft.(cur_trial).drift, ft.(cur_trial).id, vt);
        clc
    end
    
end