function indv = temporalDynamics(ii, ft, indv, nBins)

for trialType = {'cong','incong'}
    subjResp = double(ft.subj_resp(ft.(trialType{1}).id));
    targetOrientation = ft.orientation_cued_target(ft.(trialType{1}).id);
    respTimes = ft.response_time(ft.(trialType{1}).id);
    msLatency = ft.(trialType{1}).ms_latency;
    [~, ~, thresh, ~] = PartitionEqualGroups(ft.(trialType{1}).ms_latency', nBins);
    
    for threshIdx = 1:length(thresh) - 1
        fname = sprintf('%s_bin%i', trialType{1}, threshIdx);
        lowerThresh = thresh(threshIdx);
        upperThresh = thresh(threshIdx + 1);
        
        indv.temp_dynamics.trialIdx.(fname){ii} = find(msLatency > lowerThresh & msLatency <= upperThresh);
        validTrials = indv.temp_dynamics.trialIdx.(fname){ii};
        indv.temp_dynamics.(sprintf('%s_dprime', trialType{1}))(ii, threshIdx) = ...
            CalculateDprime_2(subjResp(validTrials), targetOrientation(validTrials));
        indv.temp_dynamics.(sprintf('%s_resp_time', trialType{1}))(ii, threshIdx) = nanmean(respTimes(validTrials));
    end
    
    validTrials = [indv.temp_dynamics.trialIdx.(sprintf('%s_bin1', trialType{1})){ii}]; %, indv.temp_dynamics.trialIdx.(sprintf('%s_bin2', trialType{1})){ii}];
    indv.(trialType{1}).binned_dprime(ii) = CalculateDprime_2(subjResp(validTrials), targetOrientation(validTrials));
    indv.(trialType{1}).binned_resp_time(ii) = nanmean(respTimes(validTrials));
    indv.(trialType{1}).binned_n(ii) = length(validTrials);
end

for threshIdx = 1:length(thresh) - 1
    indv.temp_dynamics.ms_latency(ii, threshIdx) = ...
        mean([ft.cong.ms_latency(indv.temp_dynamics.trialIdx.(sprintf('cong_bin%i', threshIdx)){ii}), ...
        ft.incong.ms_latency(indv.temp_dynamics.trialIdx.(sprintf('incong_bin%i', threshIdx)){ii})]);
end

indv.cong.binned_ms_latency(ii) =  mean([ft.cong.ms_latency(indv.temp_dynamics.trialIdx.cong_bin1{ii})]); %, ft.cong.ms_latency(indv.temp_dynamics.trialIdx.cong_bin2{ii})]);
indv.incong.binned_ms_latency(ii) =  mean([ft.incong.ms_latency(indv.temp_dynamics.trialIdx.incong_bin1{ii})]); %, ft.incong.ms_latency(indv.temp_dynamics.trialIdx.incong_bin2{ii})]);

indv.cong.binned_ms_latency_vector{ii} =  [ft.cong.ms_latency(indv.temp_dynamics.trialIdx.cong_bin1{ii})]; %, ft.cong.ms_latency(indv.temp_dynamics.trialIdx.cong_bin2{ii})];
indv.incong.binned_ms_latency_vector{ii} = [ft.incong.ms_latency(indv.temp_dynamics.trialIdx.incong_bin1{ii})]; %, ft.incong.ms_latency(indv.temp_dynamics.trialIdx.incong_bin2{ii})];

indv.temp_dynamics.cong_incong_delta_dprime(ii,:) = indv.temp_dynamics.cong_dprime(ii,:) - indv.temp_dynamics.incong_dprime(ii,:);
indv.temp_dynamics.cong_incong_delta_resp_time(ii,:) = indv.temp_dynamics.cong_resp_time(ii,:) - indv.temp_dynamics.incong_resp_time(ii,:);

indv.temp_dynamics.cong_neutral_delta_dprime(ii,:) = indv.temp_dynamics.cong_dprime(ii,:) - indv.neutral.dprime(ii);
indv.temp_dynamics.cong_neutral_delta_resp_time(ii,:) = indv.temp_dynamics.cong_resp_time(ii,:) - indv.neutral.resp_time(ii);

indv.temp_dynamics.incong_neutral_delta_dprime(ii,:) = indv.temp_dynamics.incong_dprime(ii,:) - indv.neutral.dprime(ii);
indv.temp_dynamics.incong_neutral_delta_resp_time(ii,:) = indv.temp_dynamics.incong_resp_time(ii,:) - indv.neutral.resp_time(ii);

end