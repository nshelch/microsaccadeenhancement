function displayTrialSequence(vt)

BackgroundColor = [0.85 0.85 0.85];
MainFigure = figure('Visible', 'on', ...
    'Name', 'Display Trial', ...
    'Position', [50 0 940 830], ...
    'NumberTitle', 'off', ...
    'Toolbar', 'figure', ...
    'Resize', 'off', 'Color', BackgroundColor);
set(MainFigure, 'PaperPositionMode','auto');
set(MainFigure, 'InvertHardcopy','off');
set(MainFigure, 'Units','pixels');
[pxAngle] = CalculatePxAngle(vt{1}.Xres, 1230, 390);
vt = osRemoverInEM(vt);
for ii = 1:length(vt)
    X = sgolayfilt(double(vt{ii}.x.position),3,41);
    Y = sgolayfilt(double(vt{ii}.y.position),3,41);
    X = vt{ii}.x.position+vt{ii}.xoffset*pxAngle;
    Y = vt{ii}.y.position+vt{ii}.yoffset*pxAngle;
    CueON = vt{ii}.TimeCueON;
    CueOFF = CueON+vt{ii}.CueTime; 
    FixationON = vt{ii}.TimeFixationON;
    FixationOFF = FixationON+vt{ii}.FixationTime; 
    TargetON = vt{ii}.TimeTargetON;    
    TargetOFF = TargetON+vt{ii}.TargetTime;
    MaskON = vt{ii}.TimeMaskON;
    MaskOFF = MaskON+vt{ii}.MaskTime;
    ResponseTime = vt{ii}.ResponseTime;
    BeepON = FixationOFF+vt{ii}.BeepDelayTime;
    BeepOFF = BeepON+vt{ii}.SaccCueTime;
    
    plot(X, 'Color', [0 0 200] / 255, 'HitTest', 'off');
    hold on
    plot(Y, 'Color', [0 180 0] / 255, 'HitTest', 'off');
    Limits = get(gca, 'YLim');
    % cue
    h_c = fill([CueON CueON ...
        CueON + CueOFF-CueON ...
        CueON + CueOFF-CueON], ...
        [Limits(1) (Limits(2) - 0.5) (Limits(2) - 0.5) Limits(1)], ...
        [255 175 100]/255, ...
        'EdgeColor', [255 175 100]/255, ...
        'LineWidth', 2, 'Clipping', 'On');
%     
        % target
        h_t = fill([TargetON TargetON ...
        TargetON + TargetOFF-TargetON ...
        TargetON + TargetOFF-TargetON], ...
        [Limits(1) (Limits(2) - 0.5) (Limits(2) - 0.5) Limits(1)], ...
        [175 230 100]/255, ...
        'EdgeColor', [175 230 100]/255, ...
        'LineWidth', 2, 'Clipping', 'On');   
    
        % beep time (signal for the saccade to start)
        h_s = fill([BeepON BeepON ...
        BeepON + BeepOFF-BeepON ...
        BeepON + BeepOFF-BeepON], ...
        [Limits(1) (Limits(2) - 0.5) (Limits(2) - 0.5) Limits(1)], ...
        [175 80 20]/255, ...
        'EdgeColor', [175 80 20]/255, ...
        'LineWidth', 2, 'Clipping', 'On');
    

             
             

%     % fixation
%     h_f = fill([FixationON FixationON ...
%         FixationON + FixationOFF-FixationON ...
%         FixationON + FixationOFF-FixationON], ...
%         [Limits(1) (Limits(2) - 0.5) (Limits(2) - 0.5) Limits(1)], ...
%         [175 80 20]/255, ...
%         'EdgeColor', [175 80 20]/255, ...
%         'LineWidth', 2, 'Clipping', 'On');
%     
%    % mask
%     h_m = fill([MaskON MaskON ...
%         MaskON + MaskOFF-MaskON ...
%         MaskON + MaskOFF-MaskON], ...
%         [Limits(1) (Limits(2) - 0.5) (Limits(2) - 0.5) Limits(1)], ...
%         [10 80 20]/255, ...
%         'EdgeColor', [10 80 20]/255, ...
%         'LineWidth', 2, 'Clipping', 'On');
%     
%     
%     % response
%     h_r = fill([ResponseTime ResponseTime ...
%         ResponseTime + ResponseTime+50-ResponseTime ...
%         ResponseTime + ResponseTime+50-ResponseTime], ...
%         [Limits(1) (Limits(2) - 0.5) (Limits(2) - 0.5) Limits(1)], ...
%         [70 100 20]/255, ...
%         'EdgeColor', [70 100 20]/255, ...
%         'LineWidth', 2, 'Clipping', 'On');
%     
    
    
    hx =  plot(X, 'Color', [0 0 200] / 255, 'HitTest', 'off');
    hy =  plot(Y, 'Color', [0 180 0] / 255, 'HitTest', 'off');
    if (vt{ii}.CueLocation == 1)
        TposR = plot([0 5000], [vt{ii}.TargetOffsetpx*pxAngle vt{ii}.TargetOffsetpx*pxAngle], 'r', 'LineWidth', 3);
        TposL = plot([0 5000], [-vt{ii}.TargetOffsetpx*pxAngle -vt{ii}.TargetOffsetpx*pxAngle], 'c', 'LineWidth', 3);
    elseif (vt{ii}.CueLocation == 2)
        TposL = plot([0 5000], [-vt{ii}.TargetOffsetpx*pxAngle -vt{ii}.TargetOffsetpx*pxAngle], 'r', 'LineWidth', 3);
        TposR = plot([0 5000], [vt{ii}.TargetOffsetpx*pxAngle vt{ii}.TargetOffsetpx*pxAngle], 'c', 'LineWidth', 3);
    end
    
    % sacc cue direction
    if vt{ii}.SaccCueType == 1
        plot(1,vt{ii}.TargetOffsetpx*pxAngle, '*', 'LineWidth', 10)
    elseif vt{ii}.SaccCueType == 2
        plot(1,-vt{ii}.TargetOffsetpx*pxAngle, '*', 'LineWidth', 10)
    elseif vt{ii}.SaccCueType == 0
        plot(1,vt{ii}.TargetOffsetpx*pxAngle, '*', 'LineWidth', 10)
        plot(1,-vt{ii}.TargetOffsetpx*pxAngle, '*', 'LineWidth', 10)
    end
                                    

%    legend([hx hy h_c h_f h_t h_r h_m], 'X position', 'Y position', 'Cue', ...
%        'Fixation', 'Target','Response','Mask', 'Location', 'NorthEast')
%    legend boxoff
   legend([hx hy h_t h_s h_c TposR], 'X position', 'Y position', 'Target','Saccade signal','Cue', 'Target location (red=cued)')
   legend boxoff
    set(gca,'FontSize', 20, 'YTick', [-50:10:50])
    grid on
    xlabel('time [ms]')
    ylabel('arcmin')
    
    plot(X, 'Color', [0 0 200] / 255, 'HitTest', 'off');
    hold on
    plot(Y, 'Color', [0 180 0] / 255, 'HitTest', 'off');
    ylim([-50 50])
    %xlim([1500 2500])
%     if (vt{ii}.TrialType==1 & sign(vt{ii}.X_TargetOffset)==1)
%     title(sprintf('Valid trial, cue to the right, RT = %.2f', vt{ii}.RT))
%     elseif  (vt{ii}.TrialType==1 & (sign(vt{ii}.X_TargetOffset)==-1))
%      title(sprintf('Valid trial, cue to the left, RT = %.2f', vt{ii}.RT))
%     elseif  (vt{ii}.TrialType==0 & (sign(vt{ii}.X_TargetOffset)==-1))
%      title(sprintf('Invalid trial, cue to the left, RT = %.2f', vt{ii}.RT))
%     elseif (vt{ii}.TrialType==0 & sign(vt{ii}.X_TargetOffset)==1)
%     title(sprintf('Invalid trial, cue to the right, RT = %.2f', vt{ii}.RT))
%     elseif  (vt{ii}.TrialType==2 & (sign(vt{ii}.X_TargetOffset)==-1))
%         title(sprintf('Neutral trial, target to the left, RT = %.2f', vt{ii}.RT))
%     elseif (vt{ii}.TrialType==2 & sign(vt{ii}.X_TargetOffset)==1)
%         title(sprintf('Neutral trial, target to the right, RT = %.2f', vt{ii}.RT))
%     elseif  (vt{ii}.TrialType==3 & (sign(vt{ii}.X_TargetOffset)==-1))
%         title(sprintf('Catch trial, cue to the left, RT = %.2f', vt{ii}.RT))
%     elseif (vt{ii}.TrialType==3 & sign(vt{ii}.X_TargetOffset)==1)
%         title(sprintf('Catch trial, cue to the right, RT = %.2f', vt{ii}.RT))
%     end

    input ''
    cla
    
    
    
    
    
end