%% Why are people doing so well in the fixation incongruent condition?

% Not all subjects do this, however, a majority of subjects (all except
% Karina) do show either an equivalent performance in the fixational
% conditions (especially fixational incongruent) as congruent condition if
% not more so (Melissa and Cody)

load('./Data/AllSubjects.mat')
subName = {'Adriana','Anne','Cody','Kamshat','Karina','Melissa'};

for ii = 1:length(subName)
    curSub = subName{ii};
    dprimeValues(ii,:) = [allSubjects.(curSub).stats.d_prime.d.cong, ...
        allSubjects.(curSub).stats.d_prime.d.neutral, ...
        allSubjects.(curSub).stats.d_prime.d.incong, ...
        allSubjects.(curSub).stats.d_prime.d.cong_no_ms, ...
        allSubjects.(curSub).stats.d_prime.d.incong_no_ms];
    trialCounts(ii,:) = [length(allSubjects.(curSub).ft.cong.id), ...
        length(allSubjects.(curSub).ft.neutral.id), ...
        length(allSubjects.(curSub).ft.incong.id), ...
        length(allSubjects.(curSub).ft.cong_no_ms.id), ...
        length(allSubjects.(curSub).ft.incong_no_ms.id)];
end

figure('position', [75, 200, 1800, 700])
yLimit = [.6, 1.6; 1, 3.25; .8, 2.2; .8, 1.8; .5, 1.8; .8, 2.2];
for ii = 1:6
    h = subplot(1, 6, ii);
    h.Position(3) = .1023;
    if ii == 1
        h.Position(1) = h.Position(1) - 0.08;
        prevFigEnd = h.Position(1) + h.Position(3);
    else
        h.Position(1) = prevFigEnd + .06;
        prevFigEnd = h.Position(1) + h.Position(3);
    end
    trialColors = parula(5);
    plot(1:5, dprimeValues(ii,:) , 'Color', [127/255, 127/255, 127/255])
    hold on
    scatter(1:5, dprimeValues(ii,:), 70, trialColors ,'filled')
    for tt = 1:5
        plot([.5 5.5], repmat(dprimeValues(ii,tt), [1,2]), 'LineStyle', '--', 'Color', trialColors(tt,:))
        text(tt - .2, dprimeValues(ii,tt) + .06, sprintf('%i',trialCounts(ii,tt)))
    end
    xlim([.5 5.5]); ylim(yLimit(ii,:)); ylabel('Sensitivity'); xtickangle(315)
    set(gca,'XTickLabel',{'Cong','Neutral','Incong','Fix Cong','Fix Incong'},'FontSize',12)
    title(sprintf('%s', subName{ii}))
end
print('-dpng', './Documents/Figures/DprimeAllSubjectsDifferentAxes.png')
print('-depsc', './Documents/Figures/DprimeAllSubjectsDifferentAxes.epsc')

%% Could this be due to proximity of their gaze during target presentation?

figure('position',[300, 200, 1500, 700])
for ii = 1:length(subName)
    curSub = subName{ii};
    gazePos{1,:} = allSubjects.(curSub).ft.cong.gaze_pos;
    gazePos{2,:} = allSubjects.(curSub).ft.neutral.gaze_pos;
    gazePos{3,:} = allSubjects.(curSub).ft.incong.gaze_pos;
    gazePos{4,:} = allSubjects.(curSub).ft.incong_no_ms.gaze_pos;
    gazePos{5,:} = allSubjects.(curSub).ft.cong_no_ms.gaze_pos;
    
    distTarget{1,:} = cell2mat(allSubjects.(curSub).ft.cong.dist_from_target);
    distTarget{2,:} = cell2mat(allSubjects.(curSub).ft.neutral.dist_from_target);
    distTarget{3,:} = cell2mat(allSubjects.(curSub).ft.incong.dist_from_target);
    distTarget{4,:} = cell2mat(allSubjects.(curSub).ft.incong_no_ms.dist_from_target);
    distTarget{5,:} = cell2mat(allSubjects.(curSub).ft.cong_no_ms.dist_from_target);
    
    subplot(2,6,ii)
    MultipleHist(gazePos, 'binfactor', 1, 'samebins','smooth','color','parula')
    xlabel('Gaze Position')
    title(sprintf('%s', curSub))
    xlim([0, 15])
    subplot(2,6,ii + 6)
    MultipleHist(distTarget, 'binfactor', 1, 'samebins','smooth','color','parula')
    title(sprintf('%s', curSub))
    xlabel('Dist from Target')
    xlim([0, 40])
    
    clear gazePos distTarget
end
h = legend({'Cong','Neutral','Incong','Fix Cong','Fix Incong'});
h.Position(1) = .015; h.Position(2) = .8;
print('-dpng', './Documents/Figures/GazePositionAllSubjects.png')
print('-depsc', './Documents/Figures/GazePositionAllSubjects.epsc')

%% Could this be due to the two targets being the same and so the task is really easy

rowNames = {'Cong', 'Neutral', 'Incong', 'Fix Cong', 'Fix Incong'};
colNames = {'T1equalT2', 'TotalTrials', 'SameTargetPerf', 'ActualPerf'};

for ii = 1:length(subName)
    curSub = subName{ii};
    congT1 = allSubjects.(curSub).ft.t1Orientation(allSubjects.(curSub).ft.cong.id);
    congT2 = allSubjects.(curSub).ft.t2Orientation(allSubjects.(curSub).ft.cong.id);
    sameTargetCong = (congT1 == congT2);
    sameTargetCongPerf = nanmean(allSubjects.(curSub).ft.cong.perf(sameTargetCong));
    
    neutralT1 = allSubjects.(curSub).ft.t1Orientation(allSubjects.(curSub).ft.neutral.id);
    neutralT2 = allSubjects.(curSub).ft.t2Orientation(allSubjects.(curSub).ft.neutral.id);
    sameTargetNeutral = (neutralT1 == neutralT2);
    sameTargetNeutralPerf = nanmean(allSubjects.(curSub).ft.neutral.perf(sameTargetNeutral));
        
    incongT1 = allSubjects.(curSub).ft.t1Orientation(allSubjects.(curSub).ft.incong.id);
    incongT2 = allSubjects.(curSub).ft.t2Orientation(allSubjects.(curSub).ft.incong.id);
    sameTargetIncong = (incongT1 == incongT2);
    sameTargetIncongPerf = nanmean(allSubjects.(curSub).ft.incong.perf(sameTargetIncong));    

    congNoMsT1 = allSubjects.(curSub).ft.t1Orientation(allSubjects.(curSub).ft.cong_no_ms.id);
    congNoMsT2 = allSubjects.(curSub).ft.t2Orientation(allSubjects.(curSub).ft.cong_no_ms.id);
    sameTargetCongNoMs = (congNoMsT1 == congNoMsT2);
    sameTargetCongNoMsPerf = nanmean(allSubjects.(curSub).ft.cong_no_ms.perf(sameTargetCongNoMs));    
        
    incongNoMsT1 = allSubjects.(curSub).ft.t1Orientation(allSubjects.(curSub).ft.incong_no_ms.id);
    incongNoMsT2 = allSubjects.(curSub).ft.t2Orientation(allSubjects.(curSub).ft.incong_no_ms.id);
    sameTargetIncongNoMs = (incongNoMsT1 == incongNoMsT2);
    sameTargetIncongNoMsPerf = nanmean(allSubjects.(curSub).ft.incong_no_ms.perf(sameTargetIncongNoMs));
    
    sameTarget = [sum(sameTargetCong); sum(sameTargetNeutral); sum(sameTargetIncong); sum(sameTargetCongNoMs); sum(sameTargetIncongNoMs)];
    totalTrials = [length(sameTargetCong); length(sameTargetNeutral); length(sameTargetIncong); length(sameTargetCongNoMs); length(sameTargetIncongNoMs)];
    samePerf = [sameTargetCongPerf; sameTargetNeutralPerf; sameTargetIncongPerf; sameTargetCongNoMsPerf; sameTargetIncongNoMsPerf];
    actualPerf = [allSubjects.(curSub).ft.cong.avg_perf; allSubjects.(curSub).ft.neutral.avg_perf; allSubjects.(curSub).ft.incong.avg_perf; ...
        allSubjects.(curSub).ft.cong_no_ms.avg_perf; allSubjects.(curSub).ft.incong_no_ms.avg_perf];
    
    tableParams.dataFormat = {'%i', 2, '%.2f', 1, '%.1f', 1};
    tableParams.completeTable = 0;
    tableParams.transposeTable = 0;
    tableParams.tableCaption = sprintf('%s', curSub);
    tableParams.tableLabel = sprintf('%s', curSub);
    T = table(sameTarget, totalTrials, samePerf * 100, actualPerf * 100, 'rowNames', rowNames, 'VariableNames', colNames);
    finalTableOutput(T, tableParams, sprintf('%s_sameTarget.txt', curSub))
    
end

%% Maybe they just got better in the neutral trials

cutOff = [1050, 1290, 1830, 0, 690, 880]; 
yLimit = [.6, 1.6; 1, 3.25; .8, 2.2; .8, 1.8; .5, 1.8; .8, 2.2];
for ii = 1:length(subName)
    curSub = subName{ii};
    NeutralId = allSubjects.(curSub).ft.neutral.id(allSubjects.(curSub).ft.neutral.id >= cutOff(ii));
    neutralCounts(ii) = length(NeutralId);
    subjResp = allSubjects.(curSub).ft.subj_resp(NeutralId);
    cuedTargetOrientation = allSubjects.(curSub).ft.orientation_cued_target(NeutralId);
    [dprimeNeutral(ii)] = CalculateDprime_2(subjResp, cuedTargetOrientation);
end

neutralColors = jet(2);
figure('position', [75, 200, 1800, 700])
for ii = 1:6
    h = subplot(1, 6, ii);
    h.Position(3) = .1023;
    if ii == 1
        h.Position(1) = h.Position(1) - 0.08;
        prevFigEnd = h.Position(1) + h.Position(3);
    else
        h.Position(1) = prevFigEnd + .06;
        prevFigEnd = h.Position(1) + h.Position(3);
    end
    plot(1:2, [dprimeValues(ii,3), dprimeNeutral(ii)], 'Color', [127/255, 127/255, 127/255])
    hold on
    scatter(1:2, [dprimeValues(ii,3), dprimeNeutral(ii)], 70, neutralColors,'filled')
    plot([.5 2.5], repmat(dprimeValues(ii,3), [1,2]), 'LineStyle', '--', 'Color', neutralColors(1,:))
    text(1 - .2, dprimeValues(ii,3) + .06, sprintf('%i', trialCounts(ii, 3)))
    plot([.5 2.5], repmat(dprimeNeutral(ii), [1,2]), 'LineStyle', '--', 'Color', neutralColors(1,:))
    text(2 - .2, dprimeNeutral(ii) + .06, sprintf('%i', neutralCounts(ii)))
    
    xlim([.5 2.5]); ylim(yLimit(ii,:)); ylabel('Sensitivity'); xtickangle(315)
    set(gca, 'XTick', [1,2], 'XTickLabel',{'Neutral', 'Fix Neutral'},'FontSize',12)
    title(sprintf('%s', subName{ii}))
end
print('-dpng', './Documents/Figures/CompareNeutralFixNeutralDprime.png')
print('-depsc', './Documents/Figures/CompareNeutralFixNeutralDprime.epsc')

figure('position', [75, 200, 1800, 700])
for ii = 1:6
    h = subplot(1, 6, ii);
    h.Position(3) = .1023;
    if ii == 1
        h.Position(1) = h.Position(1) - 0.08;
        prevFigEnd = h.Position(1) + h.Position(3);
    else
        h.Position(1) = prevFigEnd + .06;
        prevFigEnd = h.Position(1) + h.Position(3);
    end
    plot(1:5, [dprimeValues(ii,1), dprimeNeutral(ii), dprimeValues(ii,3:5)], 'Color', [127/255, 127/255, 127/255])
    hold on
    scatter(1:5, [dprimeValues(ii,1), dprimeNeutral(ii), dprimeValues(ii,3:5)], 70, trialColors,'filled')
    plot([.5 5.5], repmat(dprimeNeutral(ii), [1,2]), 'LineStyle', '--', 'Color', trialColors(2,:))
    text(2 - .2, dprimeNeutral(ii) + .06, sprintf('%i', neutralCounts(ii)))
    for tt = [1,3,4,5]
        plot([.5 5.5], repmat(dprimeValues(ii,tt), [1,2]), 'LineStyle', '--', 'Color', trialColors(tt,:))
        text(tt - .2, dprimeValues(ii,tt) + .06, sprintf('%i',trialCounts(ii,tt)))
    end
    xlim([.5 5.5]); ylim(yLimit(ii,:)); ylabel('Sensitivity'); xtickangle(315)
    set(gca,'XTickLabel',{'Cong', 'Fix Neutral', 'Incong', 'Fix Cong','Fix Incong'},'FontSize',12)
    title(sprintf('%s', subName{ii}))
end
print('-dpng', './Documents/Figures/DprimeAllSubjectsFixationNeutral.png')
print('-depsc', './Documents/Figures/DprimeAllSubjectsFixationNeutral.epsc')

cutOff = [1050, 1290, 1830, 0, 690, 880]; 
yLimit = [.6, 1.6; 1, 3.25; .8, 2.2; .8, 1.8; .5, 1.8; .8, 2.2];
for ii = 1:length(subName)
    curSub = subName{ii};
    fixCombId = [allSubjects.(curSub).ft.cong_no_ms.id, allSubjects.(curSub).ft.incong_no_ms.id];
    fixCombCounts(ii) = length(fixCombId);
    subjResp = allSubjects.(curSub).ft.subj_resp(fixCombId);
    cuedTargetOrientation = allSubjects.(curSub).ft.orientation_cued_target(fixCombId);
    [dprimeFixComb(ii)] = CalculateDprime_2(subjResp, cuedTargetOrientation);
end

figure('position', [75, 200, 1800, 700])
for ii = 1:6
    h = subplot(1, 6, ii);
    h.Position(3) = .1023;
    if ii == 1
        h.Position(1) = h.Position(1) - 0.08;
        prevFigEnd = h.Position(1) + h.Position(3);
    else
        h.Position(1) = prevFigEnd + .06;
        prevFigEnd = h.Position(1) + h.Position(3);
    end
    plot(1:4, [dprimeValues(ii,1), dprimeNeutral(ii), dprimeValues(ii,3), dprimeFixComb(ii)], 'Color', [127/255, 127/255, 127/255])
    hold on
    scatter(1:4, [dprimeValues(ii,1), dprimeNeutral(ii), dprimeValues(ii,3), dprimeFixComb(ii)], 70, trialColors(1:4,:), 'filled')
    plot([.5 4.5], repmat(dprimeNeutral(ii), [1,2]), 'LineStyle', '--', 'Color', trialColors(2,:))
    text(2 - .2, dprimeNeutral(ii) + .06, sprintf('%i', neutralCounts(ii)))
    plot([.5 4.5], repmat(dprimeFixComb(ii), [1,2]), 'LineStyle', '--', 'Color', trialColors(2,:))
    text(4 - .2, dprimeFixComb(ii) + .06, sprintf('%i', fixCombCounts(ii)))
    for tt = [1,3]
        plot([.5 4.5], repmat(dprimeValues(ii,tt), [1,2]), 'LineStyle', '--', 'Color', trialColors(tt,:))
        text(tt - .2, dprimeValues(ii,tt) + .06, sprintf('%i',trialCounts(ii,tt)))
    end
    xlim([.5 4.5]); ylim(yLimit(ii,:)); ylabel('Sensitivity'); xtickangle(315)
    set(gca,'XTickLabel',{'Cong', 'Fix Neutral', 'Incong', 'Fix Comb'},'FontSize',12)
    title(sprintf('%s', subName{ii}))
end
print('-dpng', './Documents/Figures/DprimeAllSubjectsFixationCombined.png')
print('-depsc', './Documents/Figures/DprimeAllSubjectsFixationCombined.epsc')

