for ii = 1:length(subjects)
    load(sprintf('./Data/%s_summary.mat',subjects{ii}))
    indv.cont_c(ii) = ft.num_cong_trials;
    indv.cont_i(ii) = ft.num_incong_trials;
    indv.cont_n(ii) = ft.num_neutral_trials;
    indv.bad_trial(ii) = discarded_trials.bad_trial;
end

indv.total_trials(ii) = indv.cont_c + indv.cont_n + indv.cont_i;
perc_discarded_trials = mean(indv.bad_trial./indv.total_trials) * 100;

for ii = 1:length(Subject)
    load(sprintf('./Data/%s_summary.mat',Subject{ii}))

    total_trials(ii) = sum(cell2mat(struct2cell(ft.counter))) + sum(cell2mat(struct2cell(discarded_trials)));
    bad_trials(ii) = discarded_trials.bad_trial;
    microsaccades(ii) = discarded_trials.microsaccade;
end
    