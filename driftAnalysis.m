%                 tmp_drift.x = vt{ii}.x.position(drift_start(idx_valid_drift):drift_end(idx_valid_drift));
%                 tmp_drift.y = vt{ii}.y.position(drift_start(idx_valid_drift):drift_end(idx_valid_drift));
%
%                 % Calculate velocity and check if its less than
%                 % MaxVelocity filter
%                 [~,Velocity] = CalculateDriftVelocity(tmp_drift, 1);
%                 vel_idx = find(Velocity <= MaxVelocity, 1);
%
%                 % Calculated diffusion coef to check against
%                 % MaxDiffCoef filter
%                 [~,~,~,~,~,ind_seg,~,~] = CalculateDiffusionCoef(tmp_drift);
%                     if ~isempty(idx_valid_drift) && ...
%                         %                             ~isempty(vel_idx) && ...
%                         %                             ind_seg(end) <= MaxDiffCoef
%                         %                         cont_c_dr = cont_c_dr + 1;
%                         %                         cong.drift_ind_vel{cont_c_dr} = Velocity;
%                         %                         cong.drift_pos(cont_c_dr) = tmp_drift;
%                         % %                         cong.drift_count{cont_i_dr} = [ii, drift_start(idx_valid_drift)];
% % Drift Averages
% 
% [~,~,cong.dcoef_dsq,~, cong.dsq, cong.single_segment_dsq,~,~] = ...
%     CalculateDiffusionCoef(cong.drift_pos);
% [~,~,neutral.dcoef_dsq,~, neutral.dsq, neutral.single_segment_dsq,~,~] = ...
%     CalculateDiffusionCoef(neutral.drift_pos);
% [~,~,incong.dcoef_dsq,~, incong.dsq, incong.single_segment_dsq,~,~] = ...
%     CalculateDiffusionCoef(incong.drift_pos);
% [~,~,cong_no_ms.dcoef_dsq,~, cong_no_ms.dsq, cong_no_ms.single_segment_dsq,~,~] = ...
%     CalculateDiffusionCoef(cong_no_ms.drift_pos);
% [~,~,incong_no_ms.dcoef_dsq,~, incong_no_ms.dsq, incong_no_ms.single_segment_dsq,~,~] = ...
%     CalculateDiffusionCoef(incong_no_ms.drift_pos);
% 
% cong.drift_vel_avg = nanmean(cell2mat(cong.drift_ind_vel));
% neutral.drift_vel_avg = nanmean(cell2mat(neutral.drift_ind_vel));
% incong.drift_vel_avg = nanmean(cell2mat(incong.drift_ind_vel));
% cong_no_ms.drift_vel_avg = nanmean(cell2mat(cong_no_ms.drift_ind_vel));
% incong_no_ms.drift_vel_avg = nanmean(cell2mat(incong_no_ms.drift_ind_vel));
%                         %