function plot_movie_trial(vt, pxAngle, subject)

    % Setting the time intervals
    fixation_on = round(vt.TimeFixationON);
    fixation_off = round(fixation_on + vt.FixationTime);
    
    sacc_cue_on = round(fixation_off + vt.BeepDelayTime);
    sacc_cue_off = round(sacc_cue_on + vt.SaccCueTime);
    
    delay_on = sacc_cue_off;
    delay_off = delay_on + round(vt.DelayTime - vt.SaccCueTime);
    
    target_on = round(vt.TimeTargetON);
    target_off = round(target_on + vt.TargetTime);
    
    mask_on = round(vt.TimeMaskON);
    mask_off = round(mask_on + vt.MaskTime);
    
    resp_cue_on = round(vt.TimeCueON);
    resp_cue_off = round(resp_cue_on + vt.CueTime);
    
    % Getting the x and y positions (in arcmin)
    xx = vt.x.position + vt.xoffset*pxAngle;
    yy = vt.y.position + vt.yoffset*pxAngle;
    
    % Setting the time bins
    temporal_bins = length(1:5:length(xx));
    
    % Creates the structure which will save the figures for the movie
    movie_trial(temporal_bins, 1) = struct('cdata', [], 'colormap', []);
    
    % Plots the boxes for the target locations
    figure;
    box_locations;
    plot_stimuli_location(1, 14*pxAngle, 40*pxAngle) % 1 = color, 0 = no color (black and white)
    hold on
    
    pastmove = plot(xx(1), yy(1), 'b-','linewidth', 1); % What has already been plotted
    currpos = plot(xx(1), yy(1), 'k.', 'markersize', 10); % The current x and y position
    
    
    already_plotted_sacc = 0; % 1 = plotted saccade cue
    already_plotted_target = 0; % 1 = plotted targets
   
    frame_count = 0; % used to index movie_trial
    
    title(sprintf('Sacc Cue: %i, Resp Cue: %i', vt.SaccCueType, vt.CueLocation))

    for t = [1:5:length(xx), length(xx)]
        frame_count = frame_count + 1;
        
        % Plot the saccade cue
        if t >= sacc_cue_on && t <= sacc_cue_off
            if ~already_plotted_sacc
                switch vt.SaccCueType
                    case 0 % Neutral
                        sacc_line_right = plot(sacc_cue(1).x, sacc_cue(1).y, 'k-', 'linewidth', 3.5);
                        sacc_line_left = plot(sacc_cue(2).x, sacc_cue(2).y, 'k-', 'linewidth', 3.5);
                        already_plotted_sacc = 1;
                    case 1 % Right
                        sacc_line_right = plot(sacc_cue(1).x, sacc_cue(1).y, 'k-', 'linewidth', 3.5);
                        already_plotted_sacc = 1;
                    case 2 % Left
                        sacc_line_left = plot(sacc_cue(2).x, sacc_cue(2).y, 'k-', 'linewidth', 3.5);
                        already_plotted_sacc = 1;
                end
            end 
            
        % Delay period           
        elseif t >= delay_on && t <= delay_off
            switch vt.SaccCueType
                case 0
                    delete(sacc_line_right); delete(sacc_line_left);
                    delete(sacc_line_left); delete(sacc_line_right);
                case 1
                    delete(sacc_line_right);
                case 2
                    delete(sacc_line_left);
            end
            
        % Plot the targets (diagonal lines)
        elseif t >= target_on && t <= target_off
            switch vt.SaccCueType
                case 0
                    delete(sacc_line_right); delete(sacc_line_left);
                    delete(sacc_line_left); delete(sacc_line_right);
                case 1
                    delete(sacc_line_right);
                case 2
                    delete(sacc_line_left);
            end
            if ~already_plotted_target
                if vt.Target1Orientation == 135 % Left
                    target1 = plot([box(1).topLeft(1), box(1).bottomRight(1)], [box(1).topLeft(2), box(1).bottomRight(2)], 'k-', 'LineWidth', 2.5);
                elseif vt.Target1Orientation == 45 % Right
                    target1 = plot([box(1).topRight(1), box(1).bottomLeft(1)], [box(1).topRight(2), box(1).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
                end
                
                if vt.Target2Orientation == 135 % Left
                    target2 = plot([box(2).topLeft(1), box(2).bottomRight(1)], [box(2).topLeft(2), box(2).bottomRight(2)], 'k-', 'LineWidth', 2.5);
                elseif vt.Target2Orientation == 45 % Right
                    target2 = plot([box(2).topRight(1), box(2).bottomLeft(1)], [box(2).topRight(2), box(2).bottomLeft(2)], 'k-', 'LineWidth', 2.5);
                end
               
                already_plotted_target = 1;
            end
            
        % Mask (blank) interval    
        elseif t > mask_on && t < mask_off
            delete(target1); delete(target2); 
            
        % PLot the response cue    
        elseif t >= resp_cue_on && t <= resp_cue_off
            plot(resp_cue(vt.CueLocation).x, resp_cue(vt.CueLocation).y, 'k-', 'LineWidth', 3)
        end
        
        
        set(pastmove, 'XData', xx(1:t), 'YData', yy(1:t));
        set(currpos, 'XData', xx(t), 'YData', yy(t));
        
        movie_trial(frame_count) = getframe(gcf);
        pause(.001)
        
        
    end
    
    % Make video
    v_trial = VideoWriter(sprintf('./Movies/%s_Sacc_%i_Resp_%i', subject, vt.SaccCueType, vt.CueLocation), 'Motion JPEG AVI');
    open(v_trial);
    for ii = 1:temporal_bins
        writeVideo(v_trial, movie_trial(ii));
    end
    close(v_trial);

    
    
end

