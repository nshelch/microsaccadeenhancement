#include "stdafx.h" 
#include "AutoCalibration.h"

//#include "Conversion.h"
#include "emil\libc_common.hpp"
#include "emil\CMath.hpp"
#include "emil\CEnvironment.hpp"
#include "emil\CExceptions.hpp"
#include "emil\CEOS.hpp"
#include "common\EOS_common.hpp"
#include "emil\CEnvVariables.hpp"
#include "emil\CFontEngine.hpp"
///////////////////////////////////////////////////////////////////////////////////
// Private methods

///////////////////////////////////////////////////////////////////////////////////
// Calculate the fixation center of a batch of one-dimension of eyetracker data
bool AutoCalibration::_calculateCenter(const vector<float>& DataVector, float Threshold, float& Center)
{
	// If the input vector is just too small
	if (static_cast<int>(DataVector.size()) < m_paramMinValidData + m_paramReactionDelay) {
		CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Vector too small (calculateCenter)");
		return false;
	}

	double StdDev;

	vector<float>::const_iterator begin = DataVector.end();
	vector<float>::const_iterator end = DataVector.end();

	begin -= m_paramReactionDelay + m_paramMinValidData;
	end -= m_paramReactionDelay;

	StdDev = CMath::stddev(begin, end);
	if (StdDev < Threshold)	{
		Center = CMath::mean(begin, end);
		return true;
	}

	CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Unable to meet std threshold.");
	return false;

}

///////////////////////////////////////////////////////////////////////////////////
// Initialize a new calibration
void AutoCalibration::_startNewCalibration()
{
	//m_welcomeBanner->hide();
	COGLEngine::Instance()->setBackgroundColor(m_backgroundColor);

	CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_HIGHLIGHT, "Automatic calibration started");

	m_currentPoint = 0;
	m_state = STATE_POINT;

	// Activate the sample display
	enable(CExperiment::EIS_STAT1);

	// Show the first cross
	m_planeStimulus->pxSetPosition(m_xCoo[m_currentPoint], m_yCoo[m_currentPoint]);
	//m_planeStimulus->enableTrasparency(true);
	m_planeStimulus->show();

	// Clear all previous calibration data
	m_xData.clear();
	m_xData.reserve(m_paramMaxData + 25);
	m_yData.clear();
	m_yData.reserve(m_paramMaxData + 25);

	m_xCenters1.clear();
	m_xCenters1.resize(POINTS_NUM);
	m_yCenters1.clear();
	m_yCenters1.resize(POINTS_NUM);

	// Calculate the visual angles of points
	for (int i = 0; i <POINTS_NUM; i++)	{
		m_xA[i] = CConverter::Instance()->px2a(m_xCoo[i]);
		m_yA[i] = CConverter::Instance()->py2a(m_yCoo[i]);
	}

	// Start data recording
	startTrial();
}

///////////////////////////////////////////////////////////////////////////////////
// Load an old calibration
void AutoCalibration::_loadOldCalibration()
{
	if (loadCalibrationFromFile()) {

		calibrateDSP();
		saveCalibrationToFile();
		declareFinished();
	}
	else {
		CDriver_Joypad::Instance()->clearButtons();
		m_state = STATE_BADLOAD;
	}
}

///////////////////////////////////////////////////////////////////////////////////
// Public methods

///////////////////////////////////////////////////////////////////////////////////
// Default constructor
AutoCalibration::AutoCalibration(int pxWidth, int pxHeight, int RefreshRate, std::string OutputDir) :
ExCalibrator(pxWidth, pxHeight, RefreshRate)
{
	setExperimentName("Monocular Linear Auto Calibrator");

	setOutputDir(OutputDir);

	// Retrieve the name of the file that contains the stimulus to visualize
	// during the calibration
	m_fileStimulus = CEnvVariables::Instance()->getEYERISPathFile(CFG_N_CAL_STIMULUSFILE);

	// Initialize all parameters
	m_paramCalThreshold = CEnvVariables::Instance()->getFloat(CFG_N_CAL_CALSTDDEV);
	m_paramMaxData = CEnvVariables::Instance()->getInteger(CFG_N_CAL_MAXDATA);
	m_paramPrepointWait = CEnvVariables::Instance()->getInteger(CFG_N_CAL_PREPOINTWAIT);
	m_paramMinValidData = CEnvVariables::Instance()->getInteger(CFG_N_CAL_MINVALIDDATA);
	m_paramReactionDelay = CEnvVariables::Instance()->getInteger(CFG_N_CAL_REACTIONDELAY);
}

///////////////////////////////////////////////////////////////////////////////////
void AutoCalibration::eventRender(unsigned int, CEOSData* Samples)
{

	switch (m_state) {
	case STATE_QUESTION: // Show the first panel
		glColor3d(255, 255, 255);
		printCentered(CFontEngine::FONTS_ARIAL_18, 0.0f, 50.0f, "Automatic calibration procedure");
		printCentered(CFontEngine::FONTS_ARIAL_18, 0.0f, -55.0f, "Press \"L\" or left triggers to load the old calibration from file");
		printCentered(CFontEngine::FONTS_ARIAL_18, 0.0f, -80.0f, "Press \"R\" or right triggers to start a new calibration");

		//m_welcomeBanner->pxSetSize(1000,1000);
		//m_welcomeBanner->show();

		break;

	case STATE_BADLOAD: // The calibration data load was unsuccessful
		//m_welcomeBanner->show();

		glColor3d(255, 0, 0);
		printCentered(CFontEngine::FONTS_ARIAL_14, 0.0f, -55.0f, "Unable to load previous calibration");

		glColor3d(255, 255, 255);
		printCentered(CFontEngine::FONTS_ARIAL_14, 0.0f, -80.0f, "Press any key or button to continue");
		break;

	case STATE_ERROR:

		if (m_timer.isExpired()) {
			COGLEngine::Instance()->setBackgroundColor(m_backgroundColor);

			m_xData.clear();
			m_xData.reserve(m_paramMaxData + 25);
			m_yData.clear();
			m_yData.reserve(m_paramMaxData + 25);
			m_state = STATE_POINT;
			startTrial();
		}
		break;

	case STATE_POINT: // If we are recording data at the point

		// Add collected samples to our buffer
		for (int i = 0; i < Samples->samplesNumber; i++) {
			m_xData.push_back(Samples->vchannels[i][EOS_VCHANNEL_X1]);
			m_yData.push_back(Samples->vchannels[i][EOS_VCHANNEL_Y1]);
		}
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////
void AutoCalibration::eventJoypad()
{
	if (m_state == STATE_QUESTION) {
		if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_L1) ||
			CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_L2))
			_loadOldCalibration();

		else
			if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_R1) ||
				CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_R2))
				_startNewCalibration();
	}
	else if (m_state == STATE_POINT)	{
		if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_L1) ||

			CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_L2)) {
			if (m_currentPoint != 0) {

				m_currentPoint--;

				// Prepare to collect the data
				m_xData.clear();
				m_xData.reserve(m_paramMaxData + 25);
				m_yData.clear();
				m_yData.reserve(m_paramMaxData + 25);

				// Show the previous cross
				m_planeStimulus->pxSetPosition(m_xCoo[m_currentPoint], m_yCoo[m_currentPoint]);
				m_planeStimulus->show();
			}
		}
		else if (CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_R1) ||
			CDriver_Joypad::Instance()->getButtonReleased(CDriver_Joypad::JPAD_BUTTON_R2)) {

			// End recording
			endTrial();

			// Try to calculate center
			if (!_calculateCenter(m_xData, m_paramCalThreshold, m_xCenters1[m_currentPoint]) ||
				!_calculateCenter(m_yData, m_paramCalThreshold, m_yCenters1[m_currentPoint])) {
				CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_SYSTEM,
					"Calibration in point %d failed", m_currentPoint + 1);

				COGLEngine::Instance()->setBackgroundColor(255, 0, 0);
				m_timer.start(1000.f);
				m_state = STATE_ERROR;
			}
			else {

				// Print the results
				CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_SYSTEM, "     Point %d: center (%f, %f)",
					m_currentPoint + 1, m_xCenters1[m_currentPoint], m_yCenters1[m_currentPoint]);

				// Write the data out to file if enabled
				if (CEnvVariables::Instance()->getBoolean(CFG_N_CAL_SAVEPOINTS))
					saveTrial(m_dirOutput, "AUTOCAL");

				// If the calculation was successful, go on to the next point
				m_currentPoint++;

				// If all points have been calibrated, calibrate the DSP and exit
				if (m_currentPoint == POINTS_NUM) {

					// Calculate the interpolation map
					calculateMap(0, 4);

					calibrateDSP();
					saveCalibrationToFile();
					CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_HIGHLIGHT, "Calibration successful");
					CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_SYSTEM, "");

					declareFinished();
				}
				else {
					// Otherwise, go to the next calibration point
					// Prepare to collect the data
					m_xData.clear();
					m_xData.reserve(m_paramMaxData + 25);
					m_yData.clear();
					m_yData.reserve(m_paramMaxData + 25);

					// Show the next cross
					m_planeStimulus->pxSetPosition(m_xCoo[m_currentPoint], m_yCoo[m_currentPoint]);
					m_planeStimulus->show();

					// Start data recording
					startTrial();
				}
			}
		}
	}
	else
		if (m_state == STATE_BADLOAD && CDriver_Joypad::Instance()->anyButtonsReleased())
			m_state = STATE_QUESTION;
}

///////////////////////////////////////////////////////////////////////////////////
void AutoCalibration::eventKeyboard(unsigned char key, int, int)
{
	if (m_state == STATE_QUESTION) {

		// Wait for an answer
		if (key == 'l' || key == 'L')
			_loadOldCalibration();
		else if (key == 'r' || key == 'R')
			_startNewCalibration();
	}
	else if (m_state == STATE_POINT) {
		if (key == 'l' || key == 'L')
			if (m_currentPoint != 0) {

				m_currentPoint--;

				// Prepare to collect the data
				m_xData.clear();
				m_xData.reserve(m_paramMaxData + 25);
				m_yData.clear();
				m_yData.reserve(m_paramMaxData + 25);

				// Show the previous cross
				m_planeStimulus->pxSetPosition(m_xCoo[m_currentPoint], m_yCoo[m_currentPoint]);
				m_planeStimulus->show();
			}
			else if (key == 'r' || key == 'R') {

				// End recording
				endTrial();

				// Try to calculate center
				if (!_calculateCenter(m_xData, m_paramCalThreshold, m_xCenters1[m_currentPoint]) ||
					!_calculateCenter(m_yData, m_paramCalThreshold, m_yCenters1[m_currentPoint])) {

					CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_SYSTEM,
						"Calibration in point %d failed", m_currentPoint + 1);

					COGLEngine::Instance()->setBackgroundColor(255, 0, 0);
					m_timer.start(1000.f);
					m_state = STATE_ERROR;
				}
				else {

					// Print the results
					CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_SYSTEM, "     Point %d: center (%f, %f)",
						m_currentPoint + 1, m_xCenters1[m_currentPoint], m_yCenters1[m_currentPoint]);

					// Write the data out to file if enabled
					if (CEnvVariables::Instance()->getBoolean(CFG_N_CAL_SAVEPOINTS))
						saveTrial(m_dirOutput, "AUTOCAL");

					// If the calculation was successful, go on to the next point
					m_currentPoint++;

					// If all points have been calibrated, calibrate the DSP and exit
					if (m_currentPoint == POINTS_NUM) {

						// Calculate the interpolation map
						calculateMap(0, 4);

						calibrateDSP();
						saveCalibrationToFile();
						CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_SYSTEM, "");
						CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_HIGHLIGHT, "Calibration successful");
						CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_SYSTEM, "");

						declareFinished();
					}
					else {
						// Otherwise, go to the next calibration point
						// Prepare to collect the data
						m_xData.clear();
						m_xData.reserve(m_paramMaxData + 25);
						m_yData.clear();
						m_yData.reserve(m_paramMaxData + 25);

						// Show the next cross
						m_planeStimulus->pxSetPosition(m_xCoo[m_currentPoint], m_yCoo[m_currentPoint]);
						m_planeStimulus->show();

						// Start data recording
						startTrial();
					}
				}
			}
	}
	else if (m_state == STATE_BADLOAD)
		m_state = STATE_QUESTION;
}

///////////////////////////////////////////////////////////////////////////////////
void AutoCalibration::initialize()
{
	// Call its ancestor
	ExCalibrator::initialize();

	// Initialize the color background
	COGLEngine::Instance()->setBackgroundColor(102, 122, 179);

	// Create the stimulus plane, hidden for now
	//m_planeStimulus = addObject(new CImagePlane(m_fileStimulus));
	m_planeStimulus = addObject(new CSolidPlane(255, 255, 255));
	m_planeStimulus->pxSetSize(20, 20);
	m_planeStimulus->hide();

	// Set the initial point coordinates
	for (int i = 0; i < POINTS_NUM; i++) {
		// Y/X_OFFSET was originally set at 150
		m_xCoo[i] = static_cast<short>(((i % 3) - 1) * Offset);
		m_yCoo[i] = static_cast<short>(-((i / 3) - 1) * Offset);
	}

	// Set the initial state
	m_state = STATE_QUESTION;

	// Load the welcome banner
	//m_welcomeBanner = addObject(new CImagePlane(CEnvVariables::Instance()->getDirectory(CFG_N_GENERAL_ROOTDIR) + 
	//	"config/images/Window.tga"));
	//m_welcomeBanner->pxSetPosition(0, 0);
}

///////////////////////////////////////////////////////////////////////////////////
void AutoCalibration::finalize()
{
	// Call its ancestor
	ExCalibrator::finalize();
}