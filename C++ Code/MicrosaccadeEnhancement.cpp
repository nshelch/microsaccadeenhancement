// SimpleSample.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"

#include "MicrosaccadeEnhancement.h"
#include "ManualCalibration.h"
#include "AutoCalibration.h"
#include "emil-console/emil-console.hpp"
#include "ExperimentBody.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CStabAfterSaccade
BEGIN_MESSAGE_MAP(CMicrosaccadeEnhancement, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

// CMicrosaccadeEnhancement construction
CMicrosaccadeEnhancement::CMicrosaccadeEnhancement()
{
	// Initialize the manager for the configuration parameters
	m_paramsFile.addVariable(CFG_SUBJECT_NAME, std::string(""));
	m_paramsFile.addVariable(CFG_DATA_DESTINATION, std::string(""));
	m_paramsFile.addVariable(CFG_TARGET_OFFSET, 5.0f);
	m_paramsFile.addVariable(CFG_RGB_VALUE, 5.0f);
	m_paramsFile.addVariable(CFG_FIXATION_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_MASK_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_CUE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_TARGET_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_RESPONSE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_FIXSIZE, 5.0f);
	m_paramsFile.addVariable(CFG_BOXSIZE, 5.0f);
	m_paramsFile.addVariable(CFG_BOXSIZE, 5.0f);
	m_paramsFile.addVariable(CFG_TSIZEX, 5.0f);
	m_paramsFile.addVariable(CFG_TSIZEY, 5.0f);
	m_paramsFile.addVariable(CFG_SACCCUE_TIME, 5.0f);
	


	m_paramsFile.addVariable(CFG_DELAY_TIME, 5.0f);
	
	
	m_paramsFile.addVariable(CFG_X_RES, 1);
	m_paramsFile.addVariable(CFG_Y_RES, 1);
	m_paramsFile.addVariable(CFG_REFRESH_RATE, 1);
}

// The one and only CMicrosaccadeEnhancement object
CMicrosaccadeEnhancement theApp;

// CStabAfterSaccade initialization
BOOL CMicrosaccadeEnhancement::InitInstance()
{
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();
	CWinApp::InitInstance();
	AfxEnableControlContainer();

	// Load the specified configuration file
	m_paramsFile.loadFile("Params.cfg");

	// Initialize the library
	// Insert this line if the library needs to load a specific configuration file
	CWinEMIL::Instance()->initialize("c:/eyeris/latest/system/config/emil-library.cfg");

	std::string DestinationDir = m_paramsFile.getDirectory(CFG_DATA_DESTINATION) + 
		m_paramsFile.getString(CFG_SUBJECT_NAME) + "/calibration";



	// to set the initial target window
	ExTarget* target = new ExTarget(m_paramsFile.getInteger(CFG_X_RES), m_paramsFile.getInteger(CFG_Y_RES), m_paramsFile.getInteger(CFG_REFRESH_RATE));
	target->setBackgroundColor(127, 127, 127);
	CWinEMIL::Instance()->addExperiment(target);


	CWinEMIL::Instance()->addExperiment(new AutoCalibration(m_paramsFile.getInteger(CFG_X_RES), m_paramsFile.getInteger(CFG_Y_RES), m_paramsFile.getInteger(CFG_REFRESH_RATE), DestinationDir));
	CWinEMIL::Instance()->addExperiment(new ManualCalibration(m_paramsFile.getInteger(CFG_X_RES), m_paramsFile.getInteger(CFG_Y_RES), m_paramsFile.getInteger(CFG_REFRESH_RATE), DestinationDir));
		
		
    CWinEMIL::Instance()->addExperiment(new ExperimentBody(m_paramsFile.getInteger(CFG_X_RES), m_paramsFile.getInteger(CFG_Y_RES), m_paramsFile.getInteger(CFG_REFRESH_RATE), &m_paramsFile));
	
	CEMILConsole dlg("c:/eyeris/latest/system/config/emil-console.cfg");
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();

	// Clean-up the library before exiting
	CWinEMIL::Destroy();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
