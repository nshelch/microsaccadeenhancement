% Saccade Cue

sacc_cue(1).x = [0, 5]; % Right
sacc_cue(1).y = [0, 0];

sacc_cue(2).x = [0, -5]; % Left
sacc_cue(2).y = [0, 0];
    
% Response Cue

resp_cue(1).x = [21, 21];
resp_cue(1).y = [3.765, 7.265];

resp_cue(2).x = [-21, -21];
resp_cue(2).y = [3.765, 7.265];

% Box Locations (x,y)

box(1).topLeft = [17, 4];
box(1).topRight = [25, 4];
box(1).bottomLeft = [17, -4];
box(1).bottomRight = [25, -4];

box(2).topLeft = [-25, 4];
box(2).topRight = [-17, 4];
box(2).bottomLeft = [-25, -4];
box(2).bottomRight = [-17, -4];
