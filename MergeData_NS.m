clear; close all; clc;

subjects = {'Adriana','Anne','Cody','Kamshat','Karina','Melissa'};
limit = 40; 
nbin = 20;
TemporalBins = 100;
TimeLine = round(linspace(1, 309, TemporalBins));
BinsHist = -30:30;

trial_types = {'cong','neutral','incong','cong_no_ms','incong_no_ms'};

% Combining indivudual subject data
for ii = 1:length(subjects)
    
    load(sprintf('./Data/%s_summary.mat', subjects{ii}))
    
    % Statistical Tests
    for trial_idx = 1:length(trial_types)
        trial = trial_types{trial_idx}; 
        indv.(trial).dprime(ii) = stats.d_prime.d.(trial);
        indv.(trial).crit_dprime(ii) = stats.d_prime.crit.(trial);
        indv.(trial).ci_dprime(ii) = stats.d_prime.ci.(trial);
        indv.(trial).dist_from_target(ii) = nanmean(cell2mat(ft.(trial).dist_from_target));   
        indv.(trial).avg_resp_time(ii) = nanmean(ft.response_time(ft.(trial).id));
        indv.(trial).n(ii) = length(ft.(trial).id);
        %         indv.(trial).perf{ii} = ft.(trial).perf;
        indv.(trial).drift.coef(ii) = ft.(trial).drift.coef;
        indv.(trial).drift.timeDsq(ii,:) = ft.(trial).drift.timeDsq;
        indv.(trial).drift.dsq(ii,:) = ft.(trial).drift.dsq;
        indv.(trial).drift.velocity(ii) = ft.(trial).drift.velocity;
    end
    indv.fix_neutral.dprime(ii) = stats.d_prime.d.fix_neutral;
    indv.error_cong_no_ms.dprime(ii) = stats.d_prime.d.error_cong_no_ms;
    indv.error_incong_no_ms.dprime(ii) = stats.d_prime.d.error_incong_no_ms;
    indv.fix_neutral.ci_dprime(ii) = stats.d_prime.ci.fix_neutral;
    indv.error_cong_no_ms.ci_dprime(ii) = stats.d_prime.ci.error_cong_no_ms;
    indv.error_incong_no_ms.ci_dprime(ii) = stats.d_prime.ci.error_incong_no_ms;
    indv.non_fix_neutral.dprime(ii) = stats.d_prime.d.non_fix_neutral;

    indv.total_trials(ii) = ft.counter.total_trials;

    
    for trial_idx = [1,3]
        trial = trial_types{trial_idx};
        
        % Microsaccade Latency
        indv.(trial).median_ms_latency(ii) = median(ft.ms_reaction_time(ft.(trial).id));
        
        % D primes
        subj_resp = ft.subj_resp(ft.(trial).id);
        target_orientation = ft.orientation_cued_target(ft.(trial).id);
        
        % D Prime: Landing Error
        small_ms_lat_idx = find(ft.(trial).landing_error < 5);
        large_ms_lat_idx = find(ft.(trial).landing_error > 8);
        indv.(trial).small_le_dprime(ii) = CalculateDprime_2(subj_resp(small_ms_lat_idx), target_orientation(small_ms_lat_idx));
        indv.(trial).far_le_dprime(ii) = CalculateDprime_2(subj_resp(large_ms_lat_idx), target_orientation(large_ms_lat_idx));
        indv.(trial).landing_error(ii) = nanmean(ft.(trial).landing_error);
        indv.(trial).performance(ii) = sum(ft.(trial).perf)/length(ft.(trial).perf);
        
        % D Prime: Sensitivity as a Function of Ms Onset
        [~, ~, sens_thresh, ~] = PartitionEqualGroups(ft.(trial).ms_latency', 4);
        for thresh_idx = 1:length(sens_thresh) - 1
            sens_bins = sprintf('t%d_t%d_idx', thresh_idx, thresh_idx + 1);
            indv.(trial).(sens_bins) = find(ft.(trial).ms_latency > sens_thresh(thresh_idx) & ft.(trial).ms_latency <= sens_thresh(thresh_idx + 1));
            indv.(trial).(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(ii) = CalculateDprime_2(subj_resp(indv.(trial).(sens_bins)), target_orientation(indv.(trial).(sens_bins)));
        end
        indv.(trial).v_early_dprime(ii) = CalculateDprime_2(ft.subj_resp(ft.(trial).v_early_ms_id), ft.orientation_cued_target(ft.(trial).v_early_ms_id));
        % Distance from Center of Display
        h = histogram([reshape(ft.(trial).heatmap(2).xx(:,100:250), 1, size(ft.(trial).heatmap(2).xx(:,100:250), 1)*151 )...
            reshape(ft.(trial).heatmap(1).xx(:,100:250),1,size(ft.(trial).heatmap(1).xx(:,100:250),1)*151)], BinsHist);
        indv.(trial).dist_from_center(ii,:) = h.Values./sum(h.Values);
        indv.(trial).avg_dist_from_center(ii) = nanmean([reshape(ft.(trial).heatmap(2).xx(:,100:250), 1, size(ft.(trial).heatmap(2).xx(:,100:250), 1)*151 )...
            reshape(ft.(trial).heatmap(1).xx(:,100:250),1,size(ft.(trial).heatmap(1).xx(:,100:250),1)*151)]);
        
    end
    % Landing error (le)
    indv.delta_small_le(ii) = indv.cong.small_le_dprime(ii) - indv.incong.small_le_dprime(ii);
    indv.delta_far_le(ii) = indv.cong.far_le_dprime(ii) - indv.incong.far_le_dprime(ii);

    % Sensitivity as a function of ms onset
    for thresh_idx = 1:length(sens_thresh) - 1
        indv.(sprintf('t%d_t%d_ms_latency', thresh_idx, thresh_idx + 1))(ii) = ...
            mean([ft.cong.ms_latency(indv.cong.(sprintf('t%d_t%d_idx', thresh_idx, thresh_idx + 1))), ...
            ft.incong.ms_latency(indv.incong.(sprintf('t%d_t%d_idx', thresh_idx, thresh_idx + 1)))]);

        indv.(sprintf('cong_incong_delta_t%d_t%d_dprime', thresh_idx, thresh_idx + 1))(ii) = ...
            indv.cong.(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(ii) - ...
            indv.incong.(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(ii);
        
        indv.(sprintf('cong_neut_delta_t%d_t%d_dprime', thresh_idx, thresh_idx + 1))(ii) = ...
            indv.cong.(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(ii) - ...
            indv.neutral.dprime(ii);
        
        indv.(sprintf('incong_neut_delta_t%d_t%d_dprime', thresh_idx, thresh_idx + 1))(ii) = ...
            indv.incong.(sprintf('t%d_t%d_dprime',thresh_idx, thresh_idx + 1))(ii) - ...
            indv.neutral.dprime(ii);
    end
    indv.v_early_ms_latency(ii) = mean([ft.cong.v_early_ms_latency, ft.incong.v_early_ms_latency]);
    indv.cong_incong_delta_v_early_dprime(ii) = indv.cong.v_early_dprime(ii) - indv.incong.v_early_dprime(ii);
    indv.cong_neut_delta_v_early_dprime(ii) = indv.cong.v_early_dprime(ii) - indv.neutral.dprime(ii);
    indv.incong_neut_delta_v_early_dprime(ii) = indv.incong.v_early_dprime(ii) - indv.neutral.dprime(ii);
    
     % MS Latency
    hist_struct{ii} = [ft.ms_reaction_time(ft.cong.id), ft.ms_reaction_time(ft.incong.id)];
    indv.ms_latency(ii) = nanmean([ft.ms_reaction_time(ft.cong.id), ft.ms_reaction_time(ft.incong.id)]);
    
    % MS Rate
    indv.ms_rate_fixation(ii) = nanmean(ft.ms_rate_fixation);
    indv.std_ms_rate_fixation(ii) = nanstd(ft.ms_rate_fixation);
    indv.ms_rate_stimulus(ii) = nanmean(ft.ms_rate_stimulus);
    indv.std_ms_rate_stimulus(ii) = nanstd(ft.ms_rate_stimulus);

    % MS Landing Error
    indv.ms_landing_error_left_std(ii) = nansem(ft.landing_error_left, length(ft.landing_error_left));
    indv.ms_landing_error_left_x_std(ii) = nansem(ft.landing_error_left_x, length(ft.landing_error_left_x));
    indv.ms_landing_error_left_y_std(ii) = nansem(ft.landing_error_left_y, length(ft.landing_error_left_y));
    indv.avg_landing_error_left(ii) = ft.avg_landing_error_left;
    indv.avg_landing_error_left_x(ii) = ft.avg_landing_error_left_x;
    indv.avg_landing_error_left_y(ii) = ft.avg_landing_error_left_y;
    
    indv.ms_landing_error_right_std(ii) = nansem(ft.landing_error_right, length(ft.landing_error_right));
    indv.ms_landing_error_right_x_std(ii) = nansem(ft.landing_error_right_x, length(ft.landing_error_right_x));
    indv.ms_landing_error_right_y_std(ii) = nansem(ft.landing_error_right_y, length(ft.landing_error_right_y));
    indv.avg_landing_error_right(ii) = ft.avg_landing_error_right;
    indv.avg_landing_error_right_x(ii) = ft.avg_landing_error_right_x;
    indv.avg_landing_error_right_y(ii) = ft.avg_landing_error_right_y;
    
    % MS Landing Position
    indv.ms_landing_position_left_x{ii} = ft.ms_end_xpos_left;
    indv.ms_landing_position_left_y{ii} = ft.ms_end_ypos_left;
    indv.avg_landing_position_left_x(ii) = nanmean(ft.ms_end_xpos_left);
    indv.avg_landing_position_left_y(ii) = nanmean(ft.ms_end_ypos_left);
    
    indv.ms_landing_position_right_x{ii} = ft.ms_end_xpos_right;
    indv.ms_landing_position_right_y{ii} = ft.ms_end_ypos_right;
    indv.avg_landing_position_right_x(ii) = nanmean(ft.ms_end_xpos_right);
    indv.avg_landing_position_right_y(ii) = nanmean(ft.ms_end_ypos_right);
    
    % Heat Maps for neutral trials and left/right targets across all trials
    gaze_heatmap_cue(:,:,ii) = histogram2([reshape(ft.cong.heatmap(2).xx(:,1:250), 1, size(ft.cong.heatmap(2).xx(:,1:250), 1) * 250), ...
        reshape(ft.cong.heatmap(1).xx(:,1:250), 1, size(ft.cong.heatmap(1).xx(:,1:250), 1) * 250), ...
        reshape(ft.incong.heatmap(2).xx(:,1:250), 1, size(ft.incong.heatmap(2).xx(:,1:250), 1) * 250), ...
        reshape(ft.incong.heatmap(1).xx(:,1:250), 1, size(ft.incong.heatmap(1).xx(:,1:250), 1) * 250)], ...
        [reshape(ft.cong.heatmap(2).yy(:,1:250), 1, size(ft.cong.heatmap(2).xx(:,1:250), 1) * 250), ...
        reshape(ft.cong.heatmap(1).yy(:,1:250), 1, size(ft.cong.heatmap(1).xx(:,1:250), 1) * 250), ...
        reshape(ft.incong.heatmap(2).yy(:,1:250), 1, size(ft.incong.heatmap(2).xx(:,1:250), 1) * 250),...
        reshape(ft.incong.heatmap(1).yy(:,1:250), 1, size(ft.incong.heatmap(1).xx(:,1:250), 1) * 250)], ...
        [-limit, limit, nbin; -limit, limit, nbin]);
    indv.gaze_heatmap_cue(:,:,ii) = double(gaze_heatmap_cue(:,:,ii)) ./ double(max(max(max(gaze_heatmap_cue(:,:,:)))));
    
    gaze_heatmap_neutral(:,:,ii) = histogram2([reshape(ft.neutral.heatmap.xx(:,1:250), 1, size(ft.neutral.heatmap.xx(:,1:250), 1) * 250)], ...
        [reshape(ft.neutral.heatmap.yy(:,1:250), 1, size(ft.neutral.heatmap.xx(:,1:250), 1) * 250)], [-limit, limit, nbin; -limit, limit, nbin]);
    indv.gaze_heatmap_neutral(:,:,ii) = double(gaze_heatmap_neutral(:,:,ii)) ./ double(max(max(max(gaze_heatmap_neutral(:,:,:)))));
    
    % Heat maps for left and right targets separately 
    X = [ft.ms_end_xpos(ft.cong.id), ft.ms_end_xpos(ft.incong.id)];
    Y = [ft.ms_end_ypos(ft.cong.id), ft.ms_end_ypos(ft.incong.id)];
    id = ft.sacc_cue_type([ft.cong.id, ft.incong.id]);
    XL = X(id == 2); YL = Y(id == 2);
    XR = X(id == 1); YR = Y(id == 1);
    
    result = histogram2(XL, YL, [-limit, limit, nbin; -limit, limit, nbin]);
    indv.gaze_heatmap_left(:,:,ii) = result./max(max(result));

    result = histogram2(XR, YR, [-limit, limit, nbin; -limit, limit, nbin]);
    indv.gaze_heatmap_right(:,:,ii) = result./max(max(result));
    
    % Distance from Center of Display
    h = histogram(reshape(ft.neutral.heatmap.xx(:,100:250),1,size(ft.neutral.heatmap.xx(:,100:250),1)*151), BinsHist);
    indv.neutral.dist_from_center(ii,:) = h.Values./sum(h.Values);
    indv.neutral.avg_dist_from_center(ii) = nanmean(reshape(ft.neutral.heatmap.xx(:,100:250),1,size(ft.neutral.heatmap.xx(:,100:250),1)*151));
    counter(:,ii) = cell2mat(struct2cell(ft.counter));
    
    % Drift Analysis
    indv.driftDCoefDsq(ii) = ft.driftDCoefDsq;
    indv.driftTimeDsq(ii,:) = ft.driftTimedsq;
    indv.driftDsq(ii,:) = ft.driftDsq;
    indv.driftVelocity(ii) = ft.driftVelocity;
    
end

trial_types = {'cong','incong','neutral','cong_no_ms','incong_no_ms','fix_neutral','error_cong_no_ms','error_incong_no_ms','non_fix_neutral'};
% Creating summary structure
for ii = 1:length(trial_types)
    trial = trial_types{ii};
    var_names = fieldnames(indv.(trial));
    for var_idx = 1:length(var_names)
        cur_var = var_names{var_idx};
        if ~strcmp(cur_var, 'drift')
            summary.(trial).(cur_var) = nanmean(indv.(trial).(cur_var));
            summary.(trial).(sprintf('se_%s', cur_var)) = nansem(indv.(trial).(cur_var), length(subjects));
        else
            drift_var = fieldnames(indv.(trial).drift);
            for drift_var_idx = 1:length(drift_var)
                cur_drift_var = drift_var{drift_var_idx};
                summary.(trial).drift.(cur_drift_var) = nanmean(indv.(trial).drift.(cur_drift_var));
                summary.(trial).drift.(sprintf('se_%s', cur_drift_var)) = nansem(indv.(trial).drift.(cur_drift_var), length(subjects));
            end 
        end
    end
end

var_names = fieldnames(indv);
for ii = 10:length(var_names)
    cur_var = var_names{ii};
    if ~iscell(indv.(cur_var))
        summary.(cur_var) = nanmean(indv.(cur_var));
        summary.(sprintf('se_%s',cur_var)) = nansem(indv.(cur_var), length(subjects));
    end
end
summary.ms_fast_latency = nanmean([indv.ms_latency(1:3), indv.ms_latency(5)]);
summary.se_ms_fast_latency = nansem([indv.ms_latency(1:3), indv.ms_latency(5)], length([indv.ms_latency(1:3), indv.ms_latency(5)]));
summary.ms_slow_latency = nanmean([indv.ms_latency(4), indv.ms_latency(6)]);
summary.se_ms_slow_latency = nansem([indv.ms_latency(4), indv.ms_latency(6)], length([indv.ms_latency(4), indv.ms_latency(6)]));

summary.ms_fast_fix_rate = nanmean([indv.ms_rate_fixation(1:3), indv.ms_rate_fixation(5)]);
summary.se_ms_fast_fix_rate = nansem([indv.ms_rate_fixation(1:3), indv.ms_rate_fixation(5)], length([indv.ms_rate_fixation(1:3), indv.ms_rate_fixation(5)]));
summary.ms_slow_fix_rate = nanmean([indv.ms_rate_fixation(4), indv.ms_rate_fixation(6)]);
summary.se_ms_slow_fix_rate = nansem([indv.ms_rate_fixation(4), indv.ms_rate_fixation(6)], length([indv.ms_rate_fixation(4), indv.ms_rate_fixation(6)]));
[~, statTest.msFixRateSlowFast] = ttest2([indv.ms_rate_fixation(1:3), indv.ms_rate_fixation(5)], [indv.ms_rate_fixation(4), indv.ms_rate_fixation(6)]);

% Dprime Stat Test
dprimeAnova = [indv.cong.dprime; indv.neutral.dprime; indv.incong.dprime]';
[statTest.anova.p.dprime, statTest.anova.tbl.dprime, statTest.anova.stats.dprime] = ...
    anova2(dprimeAnova);
[statTest.mult_comp.c.dprime,~,~] = multcompare(statTest.anova.stats.dprime);

% Response Time Stat Test
respTimeAnova = [indv.cong.avg_resp_time; indv.neutral.avg_resp_time; indv.incong.avg_resp_time]';
[statTest.anova.p.resp_time, statTest.anova.tbl.resp_time, statTest.anova.stats.resp_time] = ...
    anova2(respTimeAnova);
[statTest.mult_comp.c.resp_time,~,~] = multcompare(statTest.anova.stats.resp_time);


% Dist from center during target presentation
[~, statTest.ttest.distCongNeutral] = ttest(indv.cong.avg_dist_from_center, indv.neutral.avg_dist_from_center);
[~, statTest.ttest.distCongIncong] = ttest(indv.cong.avg_dist_from_center, indv.incong.avg_dist_from_center);
[~, statTest.ttest.distIncongNeutral] = ttest(indv.incong.avg_dist_from_center, indv.neutral.avg_dist_from_center);

[~, statTest.ttest.fixCong_fixIncong] = ttest(indv.cong_no_ms.dprime, indv.incong_no_ms.dprime);
[~, statTest.ttest.fixCong_Neutral] = ttest(indv.cong_no_ms.dprime, indv.neutral.dprime);
[~, statTest.ttest.fixIncong_Neutral] = ttest(indv.incong_no_ms.dprime, indv.neutral.dprime);
[~, statTest.ttest.tempDynamics] = ttest(indv.cong_incong_delta_t1_t2_dprime, indv.cong_incong_delta_t4_t5_dprime);

close all; clc;
diary Manuscript_Data_PrintOut.txt
ManuscriptDataPrintOut
diary off
movefile Manuscript_Data_PrintOut.txt ./Documents/Manuscript/Latex

save('./Data/across_subjects.mat','summary','indv','hist_struct','counter','statTest')



