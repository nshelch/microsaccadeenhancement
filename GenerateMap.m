function GenerateMap(result, limit, pxAngle)

% figure
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)

pcolor(linspace(-limit, limit, size(result, 1)), linspace(-limit, limit, size(result, 1)), result');

hold on
% plot([-limit limit], [0 0], '--k')
% plot([0 0], [-limit limit], '--k')
% plot((40* pxAngle), 0, 'sk', 'MarkerSize', 10,'MarkerFaceColor', 'w')
% plot((-40* pxAngle), 0, 'sk', 'MarkerSize', 10,'MarkerFaceColor', 'w')

set(gca, 'FontSize', 16) 
xlabel('X [arcmin]')
ylabel('Y [arcmin]')
axis tight
axis square
caxis([0 1])
%axis off
box off
shading interp;
fill([17.4174, 17.4174, 24.8066, 24.8066], [-3.6946, 3.6946, 3.6946, -3.6946], 'w', 'FaceColor', 'none', 'EdgeColor', 'w', 'LineWidth', .5)
fill([-17.4174, -17.4174, -24.8066, -24.8066], [-3.6946, 3.6946, 3.6946, -3.6946], 'w', 'FaceColor', 'none', 'EdgeColor', 'w', 'LineWidth', .5)

clear figure

