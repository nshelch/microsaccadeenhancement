clear; close all; clc;

subjects = {'Adriana','Anne','Cody','Karina','Kamshat','Melissa'}; %'Kamshat',
limit = 40;
nbin = 20;
TemporalBins = 100;
TimeLine = round(linspace(1, 309, TemporalBins));
BinsHist = -30:30;
BinsMsLatency = 228.5:34.5:780.5;

trial_types = {'cong','neutral','incong','cong_no_ms','incong_no_ms'};

% Combining indivudual subject data
for ii = 1:length(subjects)
    
    load(sprintf('./Data/%s_summary.mat', subjects{ii}))
    
    for trial_idx = 1:length(trial_types)
        trial = trial_types{trial_idx};
        indv.(trial).dprime(ii) = stats.d_prime.d.(trial);
        indv.(trial).resp_time(ii) = nanmean(ft.response_time(ft.(trial).id));
        indv.(trial).n(ii) = length(ft.(trial).id);
        indv.(trial).drift.coef(ii) = ft.(trial).drift.coef;
        indv.(trial).drift.timeDsq(ii,:) = ft.(trial).drift.timeDsq;
        indv.(trial).drift.dsq(ii,:) = ft.(trial).drift.dsq;
        indv.(trial).drift.span(ii) = nanmean(ft.(trial).drift.span);
        indv.(trial).drift.curvature(ii) = nanmean(ft.(trial).drift.curvature);
        indv.(trial).drift.speed(ii) = nanmean(ft.(trial).drift.speed);
        indv.(trial).drift.span(ii) = nanmean(ft.(trial).drift.span);
        indv.(trial).drift.se_speed(ii) = nanstd(ft.(trial).drift.speed);
        indv.(trial).drift.se_span(ii) = nanstd(ft.(trial).drift.span);
        indv.(trial).drift.se_curvature(ii) = nanstd(ft.(trial).drift.curvature);
        indv.(trial).drift.se_speed(ii) = nanstd(ft.(trial).drift.speed);
        indv.(trial).drift.angle{ii} = ft.(trial).drift.angle;
        indv.(trial).drift.inst_vel_x{ii} = cell2mat(ft.(trial).drift.inst_vel_x);
        indv.(trial).drift.inst_vel_y{ii} = cell2mat(ft.(trial).drift.inst_vel_y);
        indv.(trial).drift.duration(ii) = mean(ft.(trial).drift.poi(:,2) - ft.(trial).drift.poi(:,1));
        indv.(trial).drift.se_duration(ii) = std(ft.(trial).drift.poi(:,2) - ft.(trial).drift.poi(:,1));
        
        % Distance from Center Average
        indv.(trial).avg_dist_from_center(ii) = nanmean(reshape(ft.(trial).heatmap.xx(:,100:250),1,size(ft.(trial).heatmap.xx(:,100:250),1)*151));
        
        % Distance from Center Histogram
        h = histogram(reshape(ft.(trial).heatmap.xx(:,100:250),1,size(ft.(trial).heatmap.xx(:,100:250),1)*151), BinsHist);
        indv.(trial).dist_from_center(ii,:) = h.Values./sum(h.Values);
        
    end
    
    % Fixation Drift Information
    for driftTrials = {'fixation','left_sacc_cue','right_sacc_cue','left_sacc_cue_ci','right_sacc_cue_ci','left_sacc_cue_fix_ci','right_sacc_cue_fix_ci'}
        indv.(driftTrials{1}).drift.coef(ii) = ft.(driftTrials{1}).drift.coef;
        indv.(driftTrials{1}).drift.timeDsq(ii,:) = ft.(driftTrials{1}).drift.timeDsq;
        indv.(driftTrials{1}).drift.dsq(ii,:) = ft.(driftTrials{1}).drift.dsq;
        indv.(driftTrials{1}).drift.span(ii) = nanmean(ft.(driftTrials{1}).drift.span);
        indv.(driftTrials{1}).drift.curvature(ii) = nanmean(ft.(driftTrials{1}).drift.curvature);
        indv.(driftTrials{1}).drift.speed(ii) = nanmean(ft.(driftTrials{1}).drift.speed);
        indv.(driftTrials{1}).drift.se_speed(ii) = nanstd(ft.(driftTrials{1}).drift.speed);
        indv.(driftTrials{1}).drift.se_span(ii) = nanstd(ft.(driftTrials{1}).drift.span);
        indv.(driftTrials{1}).drift.se_curvature(ii) = nanstd(ft.(driftTrials{1}).drift.curvature);
        indv.(driftTrials{1}).drift.se_speed(ii) = nanstd(ft.(driftTrials{1}).drift.speed);
        indv.(driftTrials{1}).drift.angle{ii} = ft.(driftTrials{1}).drift.angle;
        indv.(driftTrials{1}).drift.inst_vel_x{ii} = cell2mat(ft.(driftTrials{1}).drift.inst_vel_x);
        indv.(driftTrials{1}).drift.inst_vel_y{ii} = cell2mat(ft.(driftTrials{1}).drift.inst_vel_y);
        indv.(driftTrials{1}).drift.x{ii} = cell2mat(cellfun(@(x) single(x), ft.(driftTrials{1}).drift.x, 'UniformOutput',0));
        indv.(driftTrials{1}).drift.y{ii} = cell2mat(cellfun(@(x) single(x), ft.(driftTrials{1}).drift.y, 'UniformOutput',0));
        % Distance from Center Average
        indv.(driftTrials{1}).avg_dist_from_center(ii) = nanmean(indv.(driftTrials{1}).drift.x{ii});
        
        % Distance from Center Histogram
        h = histogram(indv.(driftTrials{1}).drift.x{ii}, BinsHist);
        indv.(driftTrials{1}).dist_from_center(ii,:) = h.Values./sum(h.Values);

    end
    indv.fixation.drift.duration(ii) = mean(ft.fixation.drift.poi(:,2) - ft.fixation.drift.poi(:,1));
    indv.fixation.drift.se_duration(ii) = std(ft.fixation.drift.poi(:,2) - ft.fixation.drift.poi(:,1));
    indv.neutral.drift.x{ii} = cell2mat(cellfun(@(x) single(x), ft.(driftTrials{1}).drift.x, 'UniformOutput',0));
    
    % Temporal Dynamics
    indv = temporalDynamics(ii, ft, indv, 3);
    
    % Ms Reaction Times (calculated from sacc cue onset)
    indv.ms_reaction_time(ii) = nanmean(ft.ms_reaction_time);
    
    % MS Latency (calculated from sacc cue onset)
    indv.ms_latency_vector{ii} = [ft.ms_reaction_time(ft.cong.id), ft.ms_reaction_time(ft.incong.id)];
    indv.ms_latency(ii) = nanmean([ft.ms_reaction_time(ft.cong.id), ft.ms_reaction_time(ft.incong.id)]);
    indv.cong.ms_latency(ii) = nanmean(ft.ms_reaction_time(ft.cong.id));
    indv.incong.ms_latency(ii) = nanmean(ft.ms_reaction_time(ft.cong.id));
    tmp = histogram(indv.ms_latency_vector{ii}, BinsMsLatency);
    indv.across_subj_ms_latency(ii, :) = tmp.Values./sum(tmp.Values);
    
    if ismember(ii, [1,2,3,4])
        indv.ms_fast_latency(ii) = indv.ms_latency(ii);
    else
        indv.ms_slow_latency(ii - 4) = indv.ms_latency(ii);
    end
    
    % MS Rate
    indv.ms_rate_fixation(ii) = nanmean(ft.ms_rate_fixation);
    indv.std_ms_rate_fixation(ii) = nanstd(ft.ms_rate_fixation);
    indv.ms_rate_stimulus(ii) = nanmean(ft.ms_rate_stimulus);
    indv.std_ms_rate_stimulus(ii) = nanstd(ft.ms_rate_stimulus);
    if ismember(ii, [1,2,3,4])
        indv.ms_fast_fix_rate(ii) = indv.ms_rate_fixation(ii);
        indv.ms_fast_stimulus_rate(ii) = indv.ms_rate_stimulus(ii);
    else
        indv.ms_slow_fix_rate(ii - 4) = indv.ms_rate_fixation(ii);
        indv.ms_slow_stimulus_rate(ii - 4) = indv.ms_rate_stimulus(ii);
    end
    
    % MS Landing Error (calculated based on sacc cue location)
    indv.ms_landing_error_left_x_std(ii) = nanstd(ft.landing_error_left_x);
    indv.ms_landing_error_left_y_std(ii) = nanstd(ft.landing_error_left_y);
    indv.ms_landing_error_right_x_std(ii) = nanstd(ft.landing_error_right_x);
    indv.ms_landing_error_right_y_std(ii) = nanstd(ft.landing_error_right_y);
    
    indv.ms_landing_error(ii) = mean([ft.cong.landing_error, ft.incong.landing_error]);
    
    % MS Landing Position (based on ms landing location)
    indv.avg_landing_position_left_x(ii) = nanmean(ft.ms_end_xpos_left);
    indv.avg_landing_position_left_y(ii) = nanmean(ft.ms_end_ypos_left);
    indv.avg_landing_position_right_x(ii) = nanmean(ft.ms_end_xpos_right);
    indv.avg_landing_position_right_y(ii) = nanmean(ft.ms_end_ypos_right);
        
    % Heat maps for left and right targets separately
    X = [ft.ms_end_xpos(ft.cong.id), ft.ms_end_xpos(ft.incong.id)];
    Y = [ft.ms_end_ypos(ft.cong.id), ft.ms_end_ypos(ft.incong.id)];
    id = ft.sacc_cue_type([ft.cong.id, ft.incong.id]);
    XL = X(id == 2); YL = Y(id == 2);
    XR = X(id == 1); YR = Y(id == 1);
    result = histogram2(XL, YL, [-limit, limit, nbin; -limit, limit, nbin]);
    indv.gaze_heatmap_left(:,:,ii) = result./max(max(result));
    result = histogram2(XR, YR, [-limit, limit, nbin; -limit, limit, nbin]);
    indv.gaze_heatmap_right(:,:,ii) = result./max(max(result));
    
    % Trial Stats
    indv.counter.n(:, ii) = cell2mat(struct2cell(ft.counter));
    indv.total_trials(ii) = ft.counter.total_trials;   
end

indv.counter.label = {'cong';'incong';'neutral';'cong_no_ms';'incong_no_ms'; ...
    'neutral_cong';'neutral_incong';'cong_invalid';'incong_valid'; ...
    'error_cong_no_ms';'error_incong_no_ms';'fix_neutral';'total_trials'};

nestedTrials = {'cong', 'incong', 'neutral', 'cong_no_ms', 'incong_no_ms', 'temp_dynamics', 'fixation', ...
    'left_sacc_cue','right_sacc_cue','left_sacc_cue_ci','right_sacc_cue_ci','left_sacc_cue_fix_ci','right_sacc_cue_fix_ci'};
fnames = fieldnames(indv);
% Creating summary structure
for curVar = fnames'
    if any(strcmp(curVar{1}, nestedTrials))
        nestedVar = fieldnames(indv.(curVar{1}));
        for curNestedVar = nestedVar'
            % If the field is drift
            if strcmp(curNestedVar{1}, 'drift')
                driftVar = fieldnames(indv.(curVar{1}).drift);
                for curDriftVar = driftVar'
                    if ~iscell(indv.(curVar{1}).drift.(curDriftVar{1}))
                        summary.(curVar{1}).drift.(curDriftVar{1}) = nanmean(indv.(curVar{1}).drift.(curDriftVar{1}));
                        summary.(curVar{1}).drift.(sprintf('se_%s', curDriftVar{1})) = std(indv.(curVar{1}).drift.(curDriftVar{1}));
                    end
                end
            else
                % If the field isn't drift
                if ~iscell(indv.(curVar{1}).(curNestedVar{1})) && ~strcmp(curNestedVar{1}, 'trialIdx')
                    summary.(curVar{1}).(curNestedVar{1}) = nanmean(indv.(curVar{1}).(curNestedVar{1}));
                    summary.(curVar{1}).(sprintf('se_%s', curNestedVar{1})) = std(indv.(curVar{1}).(curNestedVar{1}));
                end
            end
        end
    elseif ~iscell(indv.(curVar{1})) && ~strcmp(curVar{1}, 'counter')
        summary.(curVar{1}) = nanmean(indv.(curVar{1}));
        summary.(sprintf('se_%s', curVar{1})) = std(indv.(curVar{1}));
    end
end

PerformStatisticalTests;
close all; clc;
diary Manuscript_Data_PrintOut.txt
ManuscriptDataPrintOut
diary off
movefile Manuscript_Data_PrintOut.txt ./Documents/Manuscript/Latex

save('./Data/across_subjects.mat','summary','indv','statTest')
