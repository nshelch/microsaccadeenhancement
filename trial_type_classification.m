function [trial_type, counter] = trial_type_classification(ms, xPos, cueLocation, saccCue, counter)

if ms
    if ( (xPos < 0 && cueLocation == 2 && saccCue == 2) || ...
            (xPos > 0 && cueLocation == 1 && saccCue == 1) )
        trial_type = 'cong';
    elseif ( (xPos<0 && cueLocation == 2 && saccCue == 0) || ...
            (xPos>0 && cueLocation == 1 && saccCue == 0) )
        trial_type = 'neutral_cong';
    elseif  ( (xPos>0 && cueLocation == 2 && saccCue == 0) || ...
            (xPos<0 && cueLocation == 1 && saccCue == 0) )
        trial_type = 'neutral_incong';
    elseif ( (xPos<0 && cueLocation == 1 && saccCue == 2) || ...
            (xPos>0 && cueLocation == 2 && saccCue == 1) )
        trial_type = 'incong';
    elseif ( (xPos<0 && cueLocation == 1 && saccCue == 1) || ...
            (xPos>0 && cueLocation == 2 && saccCue == 2) )
        trial_type = 'cong_invalid';
    elseif ( (xPos<0 && cueLocation == 2 && saccCue == 1) || ...
            (xPos>0 && cueLocation == 1 && saccCue == 2) )
        trial_type = 'incong_valid';
    end
else
    if ( (cueLocation == 1 && saccCue == 1) ||...
            (cueLocation == 2 && saccCue == 2) )
        trial_type = 'cong_no_ms';
    elseif saccCue == 0
        trial_type = 'neutral';
    elseif ( (cueLocation == 1 && saccCue == 2) ||...
            (cueLocation == 2 && saccCue == 1) )
        trial_type = 'incong_no_ms';
        
    end
    
end

counter.(trial_type) = counter.(trial_type) + 1;

end
