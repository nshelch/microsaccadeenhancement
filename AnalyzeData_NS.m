clc; clear; close all;

% Trial Overview:
%   DelayTime: the duration after trial starts before fixation time starts
%   FixationTime: the duration of the fixation time
%   BeepDelayTime: the variable duration before the saccade cue is presented
%   SaccCueTime: the duration of the saccade cue presentation
%   TargetTime: the duration of the target presentation
%   MaskTime: the duration of the mask after the target appeared
%   CueTime: the duration of the cue presentation

Subject = {'Adriana','Anne','Cody','Kamshat','Karina','Melissa'};
% Subject = {'Kamshat'};

for sub_idx = 1:length(Subject)
    
    load(sprintf('./Data/%s.mat', Subject{sub_idx}))
    [pxAngle] = CalculatePxAngle(vt{1}.Xres, 1230, 390);
    
    if strcmp(Subject{sub_idx}, 'Melissa')
        DelayAfterTOn = 550; % time delay (ms) after the target is on to consider a micorsaccade in the analysis
        Dist_thresh = 15; % arcmin distance from center of display in the period preceding the target appearence
        DistTarget_tresL = 0; % arcmin distance from the target  lower bound
        DistTarget_tresU = 35; % arcmin distance from the target upper bound
        MaxMsAmp = 60;
        MinMsAmp = 0;
    elseif strcmp(Subject{sub_idx}, 'Kamshat')
        DelayAfterTOn = 550;
        Dist_thresh = 10;
        DistTarget_tresL = 15;
        DistTarget_tresU = 25;
        MaxMsAmp = 30;
        MinMsAmp = 0;
    else
        DelayAfterTOn = 450;
        Dist_thresh = 10;
        DistTarget_tresL = 15;
        DistTarget_tresU = 25;
        MaxMsAmp = 30;
        MinMsAmp = 0;
    end
    
    % Initialize Counters
    [cont.cong, cont.incong, cont.neutral, cont.cong_no_ms, cont.incong_no_ms, ...
        cont.neutral_cong, cont.neutral_incong, cont.cong_invalid, cont.incong_valid, ...
        cont.error_cong_no_ms, cont.error_incong_no_ms, cont.fix_neutral, ...
        contMs, contTrials, cont_v_early_cong, cont_v_early_incong, driftCont, ...
        contMsLeft, contMsRight, contDriftRight, contDriftLeft, contDriftRightCI, ...
        contDriftLeftCI, contDriftRightFixCI, contDriftLeftFixCI] = deal(0);
    
    % Initialize vectors
    [ms_reaction_time, orientation_cued_target, ms_rate_fixation] = deal(NaN(1, length(vt)));
    
    % Initialize heatmaps
    [ft.cong.heatmap(1).xx, ft.cong.heatmap.yy, ...
        ft.incong.heatmap.xx, ft.incong.heatmap.yy, ...
        ft.neutral.heatmap.xx, ft.neutral.heatmap.yy, ...
        ft.neutral_cong.heatmap.xx, ft.neutral_cong.heatmap.yy, ...
        ft.neutral_incong.heatmap.xx, ft.neutral_incong.heatmap.yy, ...
        ft.cong_invalid.heatmap.xx, ft.cong_invalid.heatmap.yy, ...
        ft.incong_valid.heatmap.xx, ft.incong_valid.heatmap.yy, ...
        ft.cong_no_ms.heatmap.xx, ft.cong_no_ms.heatmap.yy, ...
        ft.incong_no_ms.heatmap.xx, ft.incong_no_ms.heatmap.yy] = deal([]);
    
    % Initalize discarded trials struct
    discarded_trials = discardedTrialsStruct();
    discarded_trials.v_early_ms = 0;
    
    
    for ii = 1:length(vt)
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Saves cued target orientation
        if vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 135
            orientation_cued_target(ii) = 0;
        elseif vt{ii}.(sprintf('Target%iOrientation', vt{ii}.CueLocation)) == 45
            orientation_cued_target(ii) = 1;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Finds all microsaccades that occured after saccade cue signal
        sacc_cue_on = double(vt{ii}.TimeFixationON + vt{ii}.FixationTime + vt{ii}.BeepDelayTime);
        idx_valid_ms = find(vt{ii}.microsaccades.start > sacc_cue_on);
        
        % first microsaccade after the saccade cue signal in all trials
        if ~isempty(idx_valid_ms)
            % MsLatency is a vector which tells how long it took the subject to
            % initiate a microsaccade
            ms_reaction_time(ii) = (vt{ii}.microsaccades.start(idx_valid_ms(1)) - sacc_cue_on );
        end
        
        idx_fixation_ms = find(vt{ii}.microsaccades.start < sacc_cue_on);
        if ~isempty(idx_fixation_ms)
            ms_rate_fixation(ii) = (length(idx_fixation_ms) / sacc_cue_on) * 1000;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % eliminate trials with no tracks, blinks, large saccades, no response,
        % and too short trial duration
        [output, discarded_trials] = filter_trial(vt{ii}, discarded_trials);
        if output
            
            % Trials in which the subject responded and passed a filter
            contTrials = contTrials + 1;
            idxDrift = find(vt{ii}.drifts.start < sacc_cue_on);
            driftStart = round(vt{ii}.drifts.start(idxDrift));
            driftEnd = round(vt{ii}.drifts.start(idxDrift) + vt{ii}.drifts.duration(idxDrift));
            driftEnd(driftEnd > sacc_cue_on) = sacc_cue_on;
            
            if length(driftEnd) > 1
                idxDrift = find(max(driftEnd - driftStart) == driftEnd - driftStart);
                idxDrift = idxDrift(1);
                if max(driftEnd - driftStart) > 150
                    driftCont = driftCont + 1;
                    driftX = vt{ii}.x.position(driftStart(idxDrift):driftEnd(idxDrift)) + vt{ii}.xoffset*pxAngle;
                    driftY = vt{ii}.y.position(driftStart(idxDrift):driftEnd(idxDrift)) + vt{ii}.yoffset*pxAngle;
                    ft = insert_drift_data(ft, 'fixation', driftCont, driftX, driftY);
                    ft.fixation.drift.poi(driftCont, :) = [driftStart(idxDrift), driftEnd(idxDrift)];
                    ft.fixation.id(driftCont) = ii;
                end
            end
            
            % Specify the time region of interest for the gaze heatmaps
            gaze_heatmap_time = (round(sacc_cue_on - 50):round(vt{ii}.TimeCueON));
            if length(gaze_heatmap_time) > 903
                gaze_heatmap_time = gaze_heatmap_time(1:903);
            else % Ask about this
                gaze_heatmap_time(1:903) = [gaze_heatmap_time, ...
                    (gaze_heatmap_time(end) + 1): gaze_heatmap_time(end) + 903 - length(gaze_heatmap_time)];
            end
            
            % eliminates microsaccades that occurred at the very beginning
            % of the trial and were used to recenter the gaze
            id = find(vt{ii}.microsaccades.start > 50);
            ms_start = vt{ii}.microsaccades.start(id);
            ms_end = vt{ii}.microsaccades.duration(id) + ms_start;
            ms_amp = vt{ii}.microsaccades.amplitude(id);
            
            % find the microsaccades/drifts performed 80 - 300 ms from target on
            % these two numbers can be parameters at the beginning of the code script
            target_on = vt{ii}.TimeTargetON;
            target_off = target_on + vt{ii}.TargetTime;
            idx_valid_ms = find( ms_start > (target_on) & ms_start < (target_on + DelayAfterTOn) ); % ms occured during period of interest
            idx_no_ms = find( (ms_start > (target_on - 100) & ms_start < vt{ii}.TimeCueON), 1); % no ms occuring during slightly wider period of interest
            v_early_ms = find(ms_start > 50 & ms_start <= sacc_cue_on);
            id_ms_clear = find( ms_start > (sacc_cue_on - 80) & ms_start < target_on, 1); % ms occuring prior to period of interest
            
            % check that the average gaze position when the target is on is
            % close to 0
            xx_target = vt{ii}.x.position(round(target_on - 50):round(target_on + vt{ii}.TargetTime)) + vt{ii}.xoffset * pxAngle;
            yy_target = vt{ii}.y.position(round(target_on - 50):round(target_on + vt{ii}.TargetTime)) + vt{ii}.yoffset * pxAngle;
            % gaze location
            gaze_loc = mean(sqrt(xx_target.^2 + yy_target.^2));
            
            % distance from the cued location
            dist_from_target = target_classification(xx_target, yy_target, vt{ii}.CueLocation, vt{ii}.TargetOffsetpx, pxAngle);
            
            % IF A MICROSACCADE HAS BEEN PERFORMED
            if isempty(id_ms_clear) && ...
                    mean(gaze_loc) < Dist_thresh &&...
                    mean(dist_from_target) > DistTarget_tresL && ...
                    mean(dist_from_target) < DistTarget_tresU
                
                if ~isempty(idx_valid_ms) && ...
                        ms_amp(idx_valid_ms(1)) < MaxMsAmp && ...
                        ms_amp(idx_valid_ms(1)) > MinMsAmp
                    
                    % Trials in which a microsaccade was performed
                    contMs = contMs + 1;
                    idx_stimulus_ms = find(ms_start > sacc_cue_on & ms_start < vt{ii}.TimeCueON);
                    ft.id_ms_trials(contMs) = ii;
                    ft.ms_rate_stimulus(contMs) = (length(idx_stimulus_ms) / (vt{ii}.TimeCueON - sacc_cue_on)) * 1000;
                    ft.ms_dir_vec_x(ii) = vt{ii}.x.position(ms_end(idx_valid_ms(1))) - vt{ii}.x.position(ms_start(idx_valid_ms(1)));
                    ft.ms_dir_vec_y(ii) = vt{ii}.y.position(ms_end(idx_valid_ms(1))) - vt{ii}.y.position(ms_start(idx_valid_ms(1)));
                    ft.ms_end_xpos(ii) = vt{ii}.x.position(ms_end(idx_valid_ms(1)))+ vt{ii}.xoffset*pxAngle;
                    ft.ms_end_ypos(ii) = vt{ii}.y.position(ms_end(idx_valid_ms(1)))+ vt{ii}.yoffset*pxAngle;
                    ft.landing_error(contMs) = ...
                        target_classification(ft.ms_end_xpos(end), ft.ms_end_ypos(end), vt{ii}.CueLocation, vt{ii}.TargetOffsetpx, pxAngle);
                    %                     ft.landing_error_x(contMs) = ...
                    %                         target_classification(ft.ms_end_xpos(end), 0, vt{ii}.CueLocation, vt{ii}.TargetOffsetpx, pxAngle);
                    %                     ft.landing_error_y(contMs) = ...
                    %                         target_classification(double(vt{ii}.TargetOffsetpx), ft.ms_end_ypos(end), vt{ii}.CueLocation, vt{ii}.TargetOffsetpx, pxAngle);
                    if vt{ii}.SaccCueType == 2 && ft.ms_dir_vec_x(end) < 0 % Left
                        contMsLeft = contMsLeft + 1;
                        ft.landing_error_left(contMsLeft) = ft.landing_error(contMs);
                        ft.landing_error_left_x(contMsLeft) = ft.ms_end_xpos(end) - (-double(vt{ii}.TargetOffsetpx) * pxAngle);
                        ft.landing_error_left_y(contMsLeft)= ft.ms_end_ypos(end) - 0;
                        ft.ms_end_xpos_left(contMsLeft) = ft.ms_end_xpos(ii);
                        ft.ms_end_ypos_left(contMsLeft) = ft.ms_end_ypos(ii);
                    elseif vt{ii}.SaccCueType == 1 && ft.ms_dir_vec_x(end) > 0% Right
                        contMsRight = contMsRight + 1;
                        ft.landing_error_right(contMsRight) = ft.landing_error(contMs);
                        ft.landing_error_right_x(contMsRight) = ft.ms_end_xpos(end) - (double(vt{ii}.TargetOffsetpx) * pxAngle);
                        ft.landing_error_right_y(contMsRight) = ft.ms_end_ypos(end) - 0;
                        ft.ms_end_xpos_right(contMsRight) = ft.ms_end_xpos(ii);
                        ft.ms_end_ypos_right(contMsRight) = ft.ms_end_ypos(ii);
                    end
                    
                    [trial_type, cont] = trial_type_classification(1, ft.ms_dir_vec_x(end), vt{ii}.CueLocation, vt{ii}.SaccCueType, cont);
                    
                    ft.(trial_type).perf(cont.(trial_type)) = vt{ii}.Correct;
                    ft.(trial_type).id(cont.(trial_type)) = ii;
                    ft.(trial_type).gaze_pos(cont.(trial_type)) = gaze_loc;
                    ft.(trial_type).amp(cont.(trial_type)) = ms_amp(idx_valid_ms(1));
                    ft.(trial_type).ms_latency(cont.(trial_type)) = ms_start(idx_valid_ms(1)) - sacc_cue_on;
                    ft.(trial_type).dist_from_target{cont.(trial_type)} = dist_from_target;
                    
                    if strcmp(trial_type, 'cong') || strcmp(trial_type, 'incong')
                        ft.(trial_type).landing_error(cont.(trial_type)) = ...
                            target_classification(ft.ms_end_xpos(end), ft.ms_end_ypos(end), vt{ii}.SaccCueType, vt{ii}.TargetOffsetpx, pxAngle);
                        
                        ft.(trial_type).landing_error_x(cont.(trial_type)) = ...
                            target_classification(ft.ms_end_xpos(end), 0, vt{ii}.SaccCueType, vt{ii}.TargetOffsetpx, pxAngle);
                        
                        ft.(trial_type).landing_error_y(cont.(trial_type)) = ...
                            target_classification(double(vt{ii}.TargetOffsetpx), ft.ms_end_ypos(end), vt{ii}.SaccCueType, vt{ii}.TargetOffsetpx, pxAngle);
                        
                        % Drift after saccade cue till onset of ms
                        ms_start_time = ms_start(idx_valid_ms(1));
                        driftX = vt{ii}.x.position(round(sacc_cue_on):round(ms_start_time - 1)) + vt{ii}.xoffset*pxAngle;
                        driftY = vt{ii}.y.position(round(sacc_cue_on):round(ms_start_time - 1)) + vt{ii}.yoffset*pxAngle;
                        ft = insert_drift_data(ft, trial_type, cont.(trial_type), driftX, driftY);
                        ft.(trial_type).drift.poi(cont.(trial_type), :) = [round(sacc_cue_on), round(ms_start_time - 1)];
                        if vt{ii}.SaccCueType == 2 && ft.ms_dir_vec_x(end) < 0 % Left
                            contDriftLeft = contDriftLeft + 1;
                            contDriftLeftCI = contDriftLeftCI + 1;
                            ft = insert_drift_data(ft, 'left_sacc_cue', contDriftLeft, driftX, driftY);
                            driftX = vt{ii}.x.position(round(sacc_cue_on):round(target_off)) + vt{ii}.xoffset*pxAngle;
                            driftY = vt{ii}.y.position(round(sacc_cue_on):round(target_off)) + vt{ii}.yoffset*pxAngle;
                            ft = insert_drift_data(ft, 'left_sacc_cue_ci', contDriftLeftCI, driftX, driftY);
                        elseif vt{ii}.SaccCueType == 1 && ft.ms_dir_vec_x(end) > 0% Right
                            contDriftRight = contDriftRight + 1;
                            contDriftRightCI = contDriftRightCI + 1;
                            ft = insert_drift_data(ft, 'right_sacc_cue', contDriftRight, driftX, driftY);
                            driftX = vt{ii}.x.position(round(sacc_cue_on):round(target_off)) + vt{ii}.xoffset*pxAngle;
                            driftY = vt{ii}.y.position(round(sacc_cue_on):round(target_off)) + vt{ii}.yoffset*pxAngle;
                            ft = insert_drift_data(ft, 'right_sacc_cue_ci', contDriftRightCI, driftX, driftY);
                        end
                    end
                    
                    ft.(trial_type).heatmap.xx = [ft.(trial_type).heatmap.xx; vt{ii}.x.position(gaze_heatmap_time) + vt{ii}.xoffset * pxAngle];
                    ft.(trial_type).heatmap.yy = [ft.(trial_type).heatmap.yy; vt{ii}.y.position(gaze_heatmap_time) + vt{ii}.xoffset * pxAngle];
                    
                    % NO MICROSACCADE PERFORMED
                elseif isempty(idx_no_ms)
                    
                    [trial_type, cont] = trial_type_classification(0, NaN, vt{ii}.CueLocation, vt{ii}.SaccCueType, cont);
                    
                    ft.(trial_type).perf(cont.(trial_type)) = vt{ii}.Correct;
                    ft.(trial_type).id(cont.(trial_type)) = ii;
                    ft.(trial_type).gaze_pos(cont.(trial_type)) = gaze_loc;
                    ft.(trial_type).dist_from_target{cont.(trial_type)} = dist_from_target;
                    ft.(trial_type).heatmap.xx = [ft.(trial_type).heatmap.xx; vt{ii}.x.position(gaze_heatmap_time) + vt{ii}.xoffset * pxAngle];
                    ft.(trial_type).heatmap.yy = [ft.(trial_type).heatmap.yy; vt{ii}.y.position(gaze_heatmap_time) + vt{ii}.xoffset * pxAngle];
                    
                    driftX = vt{ii}.x.position(round(target_off):round(target_off + 500)) + vt{ii}.xoffset*pxAngle;
                    driftY = vt{ii}.y.position(round(target_off):round(target_off + 500)) + vt{ii}.yoffset*pxAngle;
                    ft = insert_drift_data(ft, trial_type, cont.(trial_type), driftX, driftY);
                    ft.(trial_type).drift.poi(cont.(trial_type), :) = [round(target_off), round(target_off + 500)];
                    if strcmp(trial_type, 'cong_no_ms') || strcmp(trial_type, 'incong_no_ms')
                        if vt{ii}.SaccCueType == 2 % Left
                            contDriftLeft = contDriftLeft + 1;
                            ft = insert_drift_data(ft, 'left_sacc_cue', contDriftLeft, driftX, driftY);
                            
                            contDriftLeftFixCI = contDriftLeftFixCI + 1;
                            ft = insert_drift_data(ft, 'left_sacc_cue_fix_ci', contDriftLeftFixCI, driftX, driftY);
                        elseif vt{ii}.SaccCueType == 1 % Right
                            contDriftRight = contDriftRight + 1;
                            ft = insert_drift_data(ft, 'right_sacc_cue', contDriftRight, driftX, driftY);
                            
                            contDriftRightFixCI = contDriftRightFixCI + 1;
                            ft = insert_drift_data(ft, 'right_sacc_cue_fix_ci', contDriftRightFixCI, driftX, driftY);
                        end   
                    end
                elseif ~isempty(v_early_ms) && isempty(idx_valid_ms)
                    % Trials in which a microsaccade was performed
                    
                    ms_idx = v_early_ms(1);
                    discarded_trials.v_early_ms = discarded_trials.v_early_ms + 1;
                    ft.ms_dir_vec_x(ii) = vt{ii}.x.position(ms_end(ms_idx(1))) - vt{ii}.x.position(ms_start(ms_idx(1)));
                    ft.ms_dir_vec_y(ii) = vt{ii}.y.position(ms_end(ms_idx(1))) - vt{ii}.y.position(ms_start(ms_idx(1)));
                    
                    % CONGRUENT TRIALS: ms = target
                    if ( (ft.ms_dir_vec_x(end) < 0 && vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 2) || ...
                            (ft.ms_dir_vec_x(end) > 0 && vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 1) )
                        
                        cont_v_early_cong = cont_v_early_cong + 1;
                        ft.cong.v_early_ms_perf(cont_v_early_cong) = vt{ii}.Correct;
                        ft.cong.v_early_ms_id(cont_v_early_cong) = ii;
                        ft.cong.v_early_ms_latency(cont_v_early_cong) = target_on - ms_start(ms_idx);
                        
                        % INCONGRUENT TRIALS: ms != target
                    elseif ( (ft.ms_dir_vec_x(end) < 0 && vt{ii}.CueLocation == 1 && vt{ii}.SaccCueType == 2) || ...
                            (ft.ms_dir_vec_x(end) > 0 && vt{ii}.CueLocation == 2 && vt{ii}.SaccCueType == 1) )
                        
                        cont_v_early_incong = cont_v_early_incong + 1;
                        ft.incong.v_early_ms_perf(cont_v_early_incong) = vt{ii}.Correct;
                        ft.incong.v_early_ms_id(cont_v_early_incong) = ii;
                        ft.incong.v_early_ms_latency(cont_v_early_incong) = target_on - ms_start(ms_idx);
                        
                    end
                else
                    discarded_trials.late_ms = discarded_trials.late_ms + 1;
                end
            else
                if ~isempty(id_ms_clear)
                    discarded_trials.microsaccade = discarded_trials.microsaccade + 1;
                    
                elseif ~(mean(gaze_loc) < Dist_thresh)
                    discarded_trials.gaze_off_center = discarded_trials.gaze_off_center + 1;
                    
                elseif ~(mean(dist_from_target) > DistTarget_tresL) || ~(mean(dist_from_target) < DistTarget_tresU)
                    discarded_trials.target_distance = discarded_trials.target_distance + 1;
                end
            end
        end
    end
    
    subj_resp = cellfun(@(z) z(:).Response, vt);
    sacc_cue_type = cellfun(@(z) z(:).SaccCueType, vt);
    cue_location = cellfun(@(z) z(:).CueLocation, vt);
    response_time = cellfun(@(z) z(:).ResponseTime - z(:).TimeCueON - z(:).CueTime, vt);
    ft.t1Orientation = cellfun(@(z) z(:).Target1Orientation, vt);
    ft.t2Orientation = cellfun(@(z) z(:).Target2Orientation, vt);
    
    ft.avg_landing_error_left = nanmean(ft.landing_error_left);
    ft.avg_landing_error_left_x = nanmean(ft.landing_error_left_x);
    ft.avg_landing_error_left_y = nanmean(ft.landing_error_left_y);
    
    ft.avg_landing_error_right = nanmean(ft.landing_error_right);
    ft.avg_landing_error_right_x = nanmean(ft.landing_error_right_x);
    ft.avg_landing_error_right_y = nanmean(ft.landing_error_right_y);
    
    trial_types = {'cong','incong','neutral','cong_no_ms','incong_no_ms'}; %,'fix_neutral','error_cong_no_ms','error_incong_no_ms'};
    for trial_idx = 1:length(trial_types)
        cur_trial = trial_types{trial_idx};
        
        ft.(cur_trial).avg_perf = sum(ft.(cur_trial).perf)/length(ft.(cur_trial).perf);
        [stats.d_prime.d.(cur_trial), ~, stats.d_prime.ci.(cur_trial), stats.d_prime.crit.(cur_trial), stats.d_prime.var.(cur_trial)] = ...
            CalculateDprime_2(subj_resp(ft.(cur_trial).id), orientation_cued_target(ft.(cur_trial).id));
        if (~strcmp(cur_trial,'fix_neutral') && ~strcmp(cur_trial,'error_cong_no_ms') && ~strcmp(cur_trial,'error_incong_no_ms'))
            [~,~,ft.(cur_trial).drift.coef,~, ft.(cur_trial).drift.dsq, ft.(cur_trial).drift.singleSegmentDsq, ft.(cur_trial).drift.timeDsq,~] = ...
                CalculateDiffusionCoef(ft.(cur_trial).drift.position);
        end
    end
    
    %     nonFixNeutralId = ft.neutral.id(1:(end - length(ft.fix_neutral.id)));
    %     [stats.d_prime.d.non_fix_neutral, ~, stats.d_prime.ci.non_fix_neutral, stats.d_prime.crit.non_fix_neutral, stats.d_prime.var.non_fix_neutral] = ...
    %         CalculateDprime_2(subj_resp(nonFixNeutralId), orientation_cued_target(nonFixNeutralId));
    
    % Z Tests
    [~, ~, ~, ~, p_no_ms_perf]  = Z_Test(sum(ft.cong_no_ms.perf),...
        length(ft.cong_no_ms.perf),sum(ft.incong_no_ms.perf),...
        length(ft.incong_no_ms.perf));
    
    [~, ~, ~, ~, p_cong_incong_ms_perf]  = Z_Test(sum(ft.cong.perf),...
        length(ft.cong.perf),sum(ft.incong.perf),...
        length(ft.incong.perf));
    
    [~, ~, ~, ~, p_cong_ms_neutral_perf]  = Z_Test(sum(ft.cong.perf),...
        length(ft.cong.perf),sum(ft.neutral.perf),...
        length(ft.neutral.perf));
    
    stats.z_test.p_cong_incong_no_ms_perf = p_no_ms_perf;
    stats.z_test.p_cong_incong_ms_perf = p_cong_incong_ms_perf;
    stats.z_test.p_cong_neut_ms_perf = p_cong_ms_neutral_perf;
    
    
    % Drift Analysis
    for driftTrials = {'fixation','left_sacc_cue','right_sacc_cue','left_sacc_cue_ci','right_sacc_cue_ci','left_sacc_cue_fix_ci','right_sacc_cue_fix_ci'}
        [~,~,ft.(driftTrials{1}).drift.coef,~, ft.(driftTrials{1}).drift.dsq, ft.(driftTrials{1}).drift.singleSegmentDsq, ft.(driftTrials{1}).drift.timeDsq,~] = ...
            CalculateDiffusionCoef(ft.(driftTrials{1}).drift.position);
    end
    clc;
    diary(sprintf('%s_summary', Subject{sub_idx}))
    diary ON
    
    fprintf('Subject: %s\n\n', Subject{sub_idx})
    % Summary Print Outs
    fprintf('\nProportion of selected trials: %.2f\n', contMs/contTrials)
    fprintf('Prop correct cong: %.2f (n=%d) d prime: %.2f \n', ft.cong.avg_perf, cont.cong, stats.d_prime.d.cong)
    fprintf('Prop correct incong: %.2f (n=%d) d prime: %.2f \n',ft.incong.avg_perf, cont.incong, stats.d_prime.d.incong)
    fprintf('Prop correct neutral: %.2f (n=%d) d prime: %.2f \n', ft.neutral.avg_perf, cont.neutral, stats.d_prime.d.neutral)
    fprintf('Prop correct congruent no saccade: %.2f (n=%d) d prime: %.2f\n', ft.cong_no_ms.avg_perf, cont.cong_no_ms,stats.d_prime.d.cong_no_ms)
    fprintf('Prop correct incongruent no saccade: %.2f (n=%d) d prime: %.2f \n', ft.incong_no_ms.avg_perf, cont.incong_no_ms, stats.d_prime.d.incong_no_ms)
    
    % Checking trial counts
    fprintf('\n Trials discarded for blinks: %d', discarded_trials.blinks)
    fprintf('\n Trials discarded for no track: %d', discarded_trials.no_track)
    fprintf('\n Trials discarded for no response: %d', discarded_trials.no_response)
    fprintf('\n Trials discarded for trial duration: %d', discarded_trials.too_short)
    fprintf('\n Trials discarded for saccades: %d', discarded_trials.saccades)
    fprintf('\n Trials discarded for early microsaccades: %d', discarded_trials.microsaccade)
    fprintf('\n Trials discaded for gaze off center: %d', discarded_trials.gaze_off_center)
    fprintf('\n Trials discarded for target distance: %d', discarded_trials.target_distance)
    fprintf('\n Trials discarded for late microsaccade: %d', discarded_trials.late_ms)
    fprintf('\n Trials prior to filter: %d', ii)
    fprintf('\n Total trials: %d \n', sum(cell2mat(struct2cell(cont))) + sum(cell2mat(struct2cell(discarded_trials))))
    
    diary OFF
    movefile(sprintf('%s_summary', Subject{sub_idx}), './Summaries')
    %%%%%%%%%%%%%%%%%%%%%% SAVING DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    plot(ft.cong.id, 1,'ks','MarkerSize',2)
    hold on
    plot(ft.incong.id, 2,'ks','MarkerSize',2)
    plot(ft.cong_no_ms.id, 3,'ks','MarkerSize',2)
    plot(ft.incong_no_ms.id, 4,'ks','MarkerSize',2)
    set(gca,'YTick',[1,2,3,4],'YTickLabel',{'Cong.','Incong.','Fix. cong','Fix. incong'})
    ylim([0.5, 4.5])
    
    ft.response_time = response_time;
    ft.ms_reaction_time = ms_reaction_time;
    ft.ms_rate_fixation = ms_rate_fixation;
    ft.orientation_cued_target = orientation_cued_target;
    ft.sacc_cue_type = sacc_cue_type;
    ft.cue_location = cue_location;
    ft.subj_resp = subj_resp;
    cont.total_trials = length(vt);
    ft.counter = cont;
    save(sprintf('Data/%s_summary.mat', Subject{sub_idx}), 'ft', 'stats', 'discarded_trials');
    close all; clc; clearvars -except sub_idx Subject
end


%%
%%%%%%%%%%%%%%%%%%%%%% SENSITIVITY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% figure('Position', [500, 400, 900, 500]);
% v_dprime = [stats.d_prime.d.cong, stats.d_prime.d.neutral, ...
%     stats.d_prime.d.incong, stats.d_prime.d.cong_no_ms, stats.d_prime.d.incong_no_ms];
% ci_dprime = [stats.d_prime.ci.cong, stats.d_prime.ci.neutral, ...
%     stats.d_prime.ci.incong, stats.d_prime.ci.cong_no_ms, stats.d_prime.ci.incong_no_ms];
% hold on
% if sig_ms
%     text(1, 2, '*', 'FontSize', 30)
% end
% errorbar(v_dprime,ci_dprime, 'ok','LineStyle', 'none', 'LineWidth',2)
% set(gca, 'FontSize', 15,'XTick', [1 2 3 4 5], 'XTickLabel', {'Cong', 'Neutral', 'Incong', 'Cong Fix', 'Incong Fix'}, ...
%     'YTick', 0:.5:3)
% ylabel('Sensitivity [d'']')
% ylim([0 3])
% xlim([0.5 5.8])
% box off
% print2eps(sprintf('./Report/Figures/%s',Subject{sub_idx}));
%
% ft.fig.v_dprime = v_dprime;
% ft.fig.ci_dprime = ci_dprime;
%
% %%%%%%%%%%%%%%%%%%%%%% PROPORTION CORRECT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% figure('Position', [500, 400, 900, 500]);
% v_perf = [cong.avg_perf, neutral.avg_perf, incong.avg_perf, cong_no_ms.avg_perf, incong_no_ms.avg_perf];
% std_perf = [sqrt((cong.avg_perf * (cong.avg_perf - 1)) / length(cong.perf)), ...
%     sqrt((neutral.avg_perf * (neutral.avg_perf - 1)) / length(neutral.perf)), ...
%     sqrt((incong.avg_perf * (incong.avg_perf - 1)) / length(incong.perf)), ...
%     sqrt((cong_no_ms.avg_perf * (cong_no_ms.avg_perf - 1)) / length(cong_no_ms.perf)), ...
%     sqrt((incong_no_ms.avg_perf * (incong_no_ms.avg_perf - 1)) / length(incong_no_ms.perf))];
% hold on
% if p_cong_incong_ms_perf < 0.05
%     text(1, .8, '*', 'FontSize', 30)
% end
% title('Performance')
% errorbar(v_perf, std_perf, 'ok','LineStyle', 'none', 'LineWidth',2)
% set(gca, 'FontSize', 15, 'XTick', [1 2 3 4 5], 'XTickLabel', {'Cong', 'Neutral', 'Incong', 'Cong Fix', 'Incong Fix'}, ...
%     'YTick', 0:.1:1)
% ylabel('Proportion correct')
% ylim([.5 1])
% xlim([0.5 5.8])
% box off
% print2eps(sprintf('./Report/Figures/%s_percent', Subject{sub_idx}));
%
% ft.fig.v_perf = v_perf;
% ft.fig.std_perf = std_perf;
%
% %%%%%%%% MS LANDING POS HEATMAP %%%%%%%%%%%%%%%%%%%%%%%%%
%
% limit = 50;
% result = histogram2(ft.ms_dir_vec_x, ft.ms_dir_vec_y, [-limit, limit, limit; -limit, limit, limit]);
% result_norm = result./max(max(result));
% GenerateMap(result_norm, limit, pxAngle)
% print2eps(sprintf('./Report/Figures/%s_landing_ms', Subject{sub_idx}));