function finalTableOutput(finalTable, tableParams, latexFilename)

% Now use this table as input in our input struct:
input.data = finalTable;

% Set the row format of the data values, if more than one format type,
% input.dataFormat = {'%.3f',2,'%.1f',1}; three digits precision for first two columns, one digit for the last
input.dataFormat = tableParams.dataFormat;
input.dataFormatMode = 'column';

% Column alignment ('l'=left-justified, 'c'=centered,'r'=right-justified):
input.tableColumnAlignment = 'c';

% Switch table borders on (1)/off (0):
input.tableBorders = 1;

% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 0;

% Switch to include begin/end table:
input.makeCompleteTableDocument = tableParams.completeTable;

% LaTex table caption:
input.tableCaption = tableParams.tableCaption;

% LaTex table label:
input.tableLabel = tableParams.tableLabel;

% Switch transposing/pivoting your table:
input.transposeTable = tableParams.transposeTable;

if (exist(sprintf('./Documents/TableLatexFiles/%s', latexFilename), 'file') == 2)
    delete(sprintf('./Documents/TableLatexFiles/%s', latexFilename))
end
diary(sprintf('./Documents/TableLatexFiles/%s', latexFilename))
diary ON
latexTable(input);
diary OFF


end

