load('./Data/across_subjects.mat')
%% Instantaneous Drift Velocity
figure('position',[175 300 1600 450])
subplot(1,3,1)
ndhist(cell2mat(cellfun(@(x) single(x), ft.left_sacc_cue.drift.inst_vel_x, 'UniformOutput',0)), ...
cell2mat(cellfun(@(x) single(x), ft.left_sacc_cue.drift.inst_vel_y, 'UniformOutput',0)), ...
'bins', .5, 'radial', 'axis',[-150 150 -150 150], 'nr', 'filt');
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
shading interp
title('Left Sacc Cue')
subplot(1,3,2)
ndhist(cell2mat(cellfun(@(x) single(x), ft.right_sacc_cue.drift.inst_vel_x, 'UniformOutput',0)), ...
cell2mat(cellfun(@(x) single(x), ft.right_sacc_cue.drift.inst_vel_y, 'UniformOutput',0)), ...
'bins', .5, 'radial', 'axis',[-150 150 -150 150], 'nr', 'filt');
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
shading interp
title('Right Sacc Cue')
subplot(1,3,3)
ndhist(cell2mat(cellfun(@(x) single(x), ft.neutral.drift.inst_vel_x, 'UniformOutput',0)), ...
cell2mat(cellfun(@(x) single(x), ft.neutral.drift.inst_vel_y, 'UniformOutput',0)), ...
'bins', .5, 'radial', 'axis',[-150 150 -150 150], 'nr', 'filt');
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
shading interp
title('Neutral Trials')

%% Drift Angle
left_sacc_angle = cell2mat(cellfun(@(x) single(x), ft.left_sacc_cue.drift.angle, 'UniformOutput',0));
right_sacc_angle = cell2mat(cellfun(@(x) single(x), ft.right_sacc_cue.drift.angle, 'UniformOutput',0));
neutral_angle = cell2mat(cellfun(@(x) single(x), ft.neutral.drift.angle, 'UniformOutput',0));

figure;
theta = linspace(0, 2*pi, 9); % angle (theta) vector
neutralDrift = cat(2, neutral_angle); % direction vector (rho)
neutralHist = hist(neutralDrift, theta);
neutralHist(1) = neutralHist(1) + neutralHist(end); % combine 0 and 2*pi
neutralHist = neutralHist(1:end-1);% combine 0 and 2*pi
neutralHist = neutralHist / sum(neutralHist); % normalizes the histogram
p1 = polar(theta, [neutralHist(1, :), neutralHist(1, 1)], 'k');
p1Color = rgb('DarkGreen');
p1.Color = p1Color;
p1.LineWidth = 2;
hold on

leftDrift = cat(2, left_sacc_angle); % direction vector (rho)
leftHist = hist(leftDrift, theta);
leftHist(1) = leftHist(1) + leftHist(end); % combine 0 and 2*pi
leftHist = leftHist(1:end-1);% combine 0 and 2*pi
leftHist = leftHist / sum(leftHist); % normalizes the histogram
p2 = polar(theta, [leftHist(1, :), leftHist(1, 1)], 'k');
p2Color = rgb('DarkBlue');
p2.Color = p2Color;
p2.LineWidth = 2;

rightDrift = cat(2, right_sacc_angle); % direction vector (rho)
rightHist = hist(rightDrift, theta);
rightHist(1) = rightHist(1) + rightHist(end); % combine 0 and 2*pi
rightHist = rightHist(1:end-1);% combine 0 and 2*pi
rightHist = rightHist / sum(rightHist); % normalizes the histogram
p3 = polar(theta, [rightHist(1, :), rightHist(1, 1)], 'k');
p3.LineWidth = 2;
legend([p1, p2, p3], {'Neutral', 'Left Sacc Cue', 'Right Sacc Cue'}, 'Location', 'none', 'position',[.8 0.8 0 0])

%% Task v Fixation
for si = 1:length(subjects)
figure('position',[175 300 1600 450])
avgDrift = [indv.cong.binned_dprime(si), indv.neutral.dprime(si), ...
    indv.incong.binned_dprime(si), indv.cong_no_ms.dprime(si), indv.incong_no_ms.dprime(si)];
subplot(1,6,1)
errorbar(avgDrift, [0 0 0 0 0], ...
    'k', 'LineStyle', 'none', 'LineWidth', 2, ...
    'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', 'w')
set(gca, 'XTick', 1:6, 'XTickLabel', {'Cong','Neutral','Incong','Fix Cong','Fix Incong'}, 'FontSize', 12)
xtickangle(325)
xlim([0.5, 6.5])
box off
title('sensitivity')
avgDrift = [indv.fixation.drift.coef(si), indv.cong.drift.coef(si), indv.neutral.drift.coef(si), ...
    indv.incong.drift.coef(si), indv.cong_no_ms.drift.coef(si), indv.incong_no_ms.drift.coef(si)];
subplot(1,6,2)
errorbar(avgDrift, [0 0 0 0 0 0], ...
    'k', 'LineStyle', 'none', 'LineWidth', 2, ...
    'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', 'w')
set(gca, 'XTick', 1:6, 'XTickLabel', {'Fixation','Cong','Neutral','Incong','Fix Cong','Fix Incong'}, 'FontSize', 12)
xtickangle(325)
xlim([0.5, 6.5])
box off
title('coef')
driftChar = {'span','curvature','speed','duration'};
for ii = 1:length(driftChar)
    subplot(1,6,ii+2)
    curDriftChar = driftChar{ii}; seDriftChar = sprintf('se_%s', curDriftChar);
    avgDrift = [indv.fixation.drift.(curDriftChar)(si), indv.cong.drift.(curDriftChar)(si), indv.neutral.drift.(curDriftChar)(si), ...
        indv.incong.drift.(curDriftChar)(si), indv.cong_no_ms.drift.(curDriftChar)(si), indv.incong_no_ms.drift.(curDriftChar)(si)];
    stdDrift = [indv.fixation.drift.(seDriftChar)(si), indv.cong.drift.(seDriftChar)(si), indv.neutral.drift.(seDriftChar)(si), ...
        indv.incong.drift.(seDriftChar)(si), indv.cong_no_ms.drift.(seDriftChar)(si), indv.incong_no_ms.drift.(seDriftChar)(si)];
    errorbar(avgDrift, stdDrift/sqrt(6), ...
        'k', 'LineStyle', 'none', 'LineWidth', 2, ...
        'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', 'w')
    set(gca, 'XTick', 1:6, 'XTickLabel', {'Fixation','Cong','Neutral','Incong','Fix Cong','Fix Incong'}, 'FontSize', 12)
    xtickangle(325)
    xlim([0.5, 6.5])
    box off
    title(curDriftChar)
end
print('-dpng',sprintf('./Documents/Figures/%s_DriftChar.png', subjects{si}))
end
subplot(1,5,5)
hold on
timeInterval = 241; %255 is the max
h1 = shadedErrorBar(summary.fixation.drift.timeDsq(1:timeInterval)*1000, summary.fixation.drift.dsq(1:timeInterval), summary.fixation.drift.se_dsq(1:timeInterval)/sqrt(6), 'k');
h2 = shadedErrorBar(summary.cong.drift.timeDsq(1:timeInterval)*1000, summary.cong.drift.dsq(1:timeInterval), summary.cong.drift.se_dsq(1:timeInterval)/sqrt(6), 'r');
h3 = shadedErrorBar(summary.neutral.drift.timeDsq(1:timeInterval)*1000, summary.neutral.drift.dsq(1:timeInterval), summary.neutral.drift.se_dsq(1:timeInterval)/sqrt(6), 'g');
h4 = shadedErrorBar(summary.incong.drift.timeDsq(1:timeInterval)*1000, summary.incong.drift.dsq(1:timeInterval), summary.incong.drift.se_dsq(1:timeInterval)/sqrt(6), 'b');
h5 = shadedErrorBar(summary.cong_no_ms.drift.timeDsq(1:timeInterval)*1000, summary.cong_no_ms.drift.dsq(1:timeInterval), summary.cong_no_ms.drift.se_dsq(1:timeInterval)/sqrt(6), 'm');
h6 = shadedErrorBar(summary.incong_no_ms.drift.timeDsq(1:timeInterval)*1000, summary.incong_no_ms.drift.dsq(1:timeInterval), summary.incong_no_ms.drift.se_dsq(1:timeInterval)/sqrt(6), 'c');
legend([h1.mainLine, h2.mainLine, h3.mainLine, h4.mainLine, h5.mainLine, h6.mainLine], {'Fixation','Cong','Neutral','Incong','Fix Cong','Fix Incong'}, 'Location','NorthWest')

%% Left Right Histogram in x

for si = 1:length(subjects)
    figure('position',[500 250 800 600])
    driftX{1} = indv.left_sacc_cue_ci.drift.x{si};
    driftX{2} = indv.left_sacc_cue_fix_ci.drift.x{si};
    driftX{3} = indv.left_sacc_cue.drift.x{si};
    driftX{4} = indv.right_sacc_cue_ci.drift.x{si};
    driftX{5} = indv.right_sacc_cue_fix_ci.drift.x{si};
    driftX{6} = indv.right_sacc_cue.drift.x{si};
    hold on
    MultipleHist(driftX, 'binfactor', 1, 'samebins', 'smooth', 'color', 'qualitative', 'proportion')
    legend({'Left Cong/Incong','Left Fix Cong/Incong','Left Cue','Right Cong/Incong','Right Fix Cong/Incong','Right Cue'})
    title(subjects{si})
    print('-dpng',sprintf('./Documents/Figures/%s_LeftRightCiFixCi.png', subjects{si}));
end

figure('position',[350 100 1200 800])
for si = 1:length(subjects)
    driftChar{1} = indv.fixation.drift.x{si};
    driftChar{2} = indv.left_sacc_cue.drift.x{si};
    driftChar{3} = indv.right_sacc_cue.drift.x{si};
    driftChar{4} = indv.neutral.drift.x{si};
    subplot(3,2,si)
    MultipleHist(driftChar, 'binfactor', 1, 'samebins', 'smooth', 'color', 'parula', 'proportion')
    legend({'Fixation','Left Cue','Right Cue', 'Neutral'}, 'Location', 'NorthWest')
    title(subjects{si})
end